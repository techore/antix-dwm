# antiX dwm Unofficial Spin

![antiX dwm](asset/image/antix-dwm-22r2.jpg)

## Download

* https://archive.org/details/antix-22-runit-dwm-rel2_202301

**Accounts**
- root account password is root.
- demo user account password is demo.

## Audience

The primary target audience is the intermediate Linux user seeking a dynamic tiling window manager and desire to do tasks using a keyboard versus a mouse. New Linux users can use this spin, but it may require reading documentation and changing computing habits.

## About

antiX Linux is a lightweight Debian based distribution. antiX dwm Unofficial is my antiX spin with Suckless' dynamic window manager, dwm, via bakkeby's dwm-flexipatch.

The spin uses:

1. antiX 5.10+ amd64-smp kernel
2. [runit](http://smarden.org/runit/) init
3. [kitty](https://sw.kovidgoyal.net/kitty/) and [Suckless](https://suckless.org/)' terminal (st) via [st-flexipatch](https://github.com/bakkeby/st-flexipatch)
4. [fish](https://fishshell.com/) shell
5. Suckless' dynamic window manager (dwm) via [dwm-flexipatch](https://github.com/bakkeby/dwm-flexipatch)
6. [dwmblocks](https://github.com/torrinfail/dwmblocks) status bar
7. [statuscmd](https://dwm.suckless.org/patches/statuscmd/) status bar script execution
8. [rofi](https://github.com/davatorium/rofi) application launcher
9. [conky](https://github.com/brndnmtthws/conky) to display cheatsheets
10. [nvim](https://neovim.io/) text edior using [nvchad](https://nvchad.com/), [vimwiki](https://vimwiki.github.io), & [vifm.vim](https://github.com/vifm/vifm.vim)
11. [vifm](https://vifm.info) file manager

There are very few GUI applications installed. The spin by default is intended to provide a minimal but functional desktop environment for you to install your preferred applications. Default tools and applications can be uninstalled safely.

antiX dwm provides cheatsheets which are accessed using **`mod`**+**`ctrl`**+**`shift`**+**`p`**.

Experiment and enjoy the journey.

## Install process

1. Obtain materials
    - 2 GB+ USB flash drive
    - antiX dwm ISO
2. Write ISO file to USB
    - I would recommend using an existing antiX or MX Linux installation and execute either `live-usb-maker` (cli) or `live-usb-maker-gui-antix`. Live USB Maker permits persistence, adding drivers, or installing alternative kernels. Other ISO to USB creation tools will work but will not support persistance.
3. Insert USB
4. Power on and enter BIOS boot menu
5. Select to boot from USB
    - If using a computer with UEFI that supports both legacy BIOS or CMS and UEFI, their should be two boot entries related to the USB where the UEFI entry will often be prefxed with UEFI. Select the entry corresponding to the targeted installation type.
6. Login using account demo and password demo
7. Execute `startx`
8. Open Toolbox menu using **`win`**+**`ctrl`**+**`p`**
9. Type install
10. Select "antiX install" then **`enter`**
11. Enter "root" when prompted for the administrative password
12. Complete the installation
13. Power off
14. Remove USB
15. Power on and login with root
16. Verify IP address assignment using `ip addr`. If no IP address, use `sudo ceni` to configure an interface.
17. By default, the xorg opensource drivers are installed, however, you may need a proprietary driver.   
    - Identify display device using `lspci | grep -e VGA -e 3D`
    - Review https://wiki.debian.org/Xorg#Video_drivers
    - My notes on Intel and Nvidia: [display.md](doc/display.md)
    - `sudo mv /etc/X11/xorg.conf /etc/X11/xorg.conf.bak` may be necessary to "reset" xorg configuration prior to using `startx` prior to driver installation
18. `reboot` after installing additional drivers
19. Login as the user account and `startx` to test
    - If the wrong resolution is displayed, use `arandr` to review and select a supported resolution. If arandr doesn't list the correct resolution, it may be due to one of these reasons
        - Missing kernel support
        - Missing display driver (review above Debian wiki article)
        - Existing /etc/X11/xorg.conf may need to be deleted to force xorg to create a new on next `startx`
20. Initial configuration assumes 1080p. If using 1440p or 2160p, execute the corresponding shell script.
    - 1440p: `conf-1440p.sh`
    - 2160p (4k): `conf-2160p.sh`
    - If using other resolutions, use [hidpi.md](doc/hidpi.md) as a reference to customize to your preference. Contributions to include in future releases are welcome.
21. Launch nvim using alias `nv` and `sudo nv` which will immediately launch Packer and install Neovim plugins. Internet connection required. Use ':help Packer' in neovim to learn more. 
22. If laptop, `sudo antix-dwm/install/inst-laptop.sh` which will enable backlight brightness and battery blocks on the statusbar.
23. Obtain API keys for use with the airquality and weather blocks on the status bar. Comments in sb-airquality.sh and sb-weather.sh located in /usr/local/bin/ will provide the URLs to obtain keys.
24. If older computer with minimal RAM, consider replacing kitty with st using `conf-dwm-rmkitty.sh`
25. Optional: review my [install scripts](doc/install-scripts.md) created for my personal use
26. Done

To install additional software, use apt or apt-get, antiX's currated list using the Package Installer, and Synaptic. 

## Getting started

dwm uses key combinations for most activities. The **`mod`** key is not a key but an abstraction or reference to an assigned key and is central to dwm key-combos. antiX dwm assigns **`win`** as the **`mod`** key. If your keyboard does not have a **`win`** key, see the [FAQ](doc/faq.md) for instructions on using **`alt`** as the **`mod`** key.

To get you started, try the following:

1. **`mod`**+**`shift`**+**`enter`** to open a shell
2. **`mod`**+**`shift`**+**`c`** to close the selected window
3. **`mod`**+**`r`** to open the rofi application launcher
4. **`esc`** to exit rofi
5. **`mod`**+**`ctrl`**+**`p`** to launch the toolbox menu
6. Type "tas" and **`enter`** to launch Task Manager
7. This is a floating window which can be moved using **`mod`**+**`left-button`** (mouse) or resized using **`mod`**+**`right-button`**. 
8. **`mod+shift+space`** to change Task Manager from floating to a tiled window and vice versus
9. Most xorg applications support **`ctrl`**+**`q`** to close but Task Manager uses **`ctrl`**+**`w`** to close. OR
10. **`mod`**+**`shift`**+**`c`** to close the selected window
11. Open several terminals and use **`mod`** or **`mod`**+**`shift`** + **`h`**, **`j`**, **`k`**, or **`l`** to switch windows, decrease and increase window size, and swap locations.
12. **`mod`**+**`num`** or **`mod`**+**`shift`**+**`num`** will switch to the numbered workspace or move the selected window to the numbered workspace.
13. **`mod`**+**`shift`**+**`ctrl`**+**`p`** to launch the Cheatsheet menu and select an alternative cheatsheet for conky to display. 
14. Use **`mod`**+**`shift`**+**`p`** to launch the Power menu and select screen lock, reset or quit dwm, or reboot or poweroff the computer.

## Supplemental documents 

1. [Frequently asked questions](doc/faq.md)
2. [Change log](changelog.md)
3. [Display](doc/display.md) drivers
4. [HiDPI](doc/hidpi.md)
5. [Install scripts](doc/install-scripts.md)
6. [Themes](doc/themes.md)
7. [Build](doc/build.md)
8. [Known issues](doc/known-issues.md)
