#!/usr/bin/env bash
# https://askubuntu.com/questions/819300/bash-how-to-look-for-a-mouse-in-bash

declare -i ID
ID=<code>xinput list | grep -Eio '(touchpad|glidepoint)\s*id\=[0-9]{1,2}' | grep -Eo '[0-9]{1,2}'</code>

if [ -n "$(ls /dev/input/by-id/*mouse 2>/dev/null | grep -i 'usb\|PS\|COM')" ]
then echo "antiX found a mouse, disabling touchpad"
 xinput disable $ID
else 
 echo "No mice around, trying to enable touchpad"
 xinput enable $ID
fi
