# Keymap & Touchpad

If fn + f# is not working and xev shows the same key depressed with or without
the use of the func key, XF86TouchpadToggle can be reassigned. However, it
original function is lost.

    ------------------------------------------------------------------------
    xmodmap -pke > .xmodmap.orig  # backup
    xmodmap -pke | grep TouchpadToggle  # results with keycode 199
    xev |grep keysum  # pressing fn+f9 results with f9, no distinction
    between fn+f9 and f9
    xmodmap -pke | grep f9  # results with keycode 75
    xmodmap -e "keycode 75 = XF86TouchpadToggle"
    ------------------------------------------------------------------------

## References

    - https://wiki.archlinux.org/index.php/Touchpad_Synaptics#Software_toggle
    - https://askubuntu.com/questions/1140686/cant-lock-touchpad-on-ubuntu-18-04-2-lts
    - https://askubuntu.com/questions/29603/how-do-i-clear-xmodmap-settings
    - https://web.archive.org/web/20170825051821/http://madduck.net:80/docs/extending-xkb/
