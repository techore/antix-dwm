# Building Debian packages

1. Install build virtual machine; debian or antix?
1. Install dependencies `apt install dpkg-dev devscripts` <-- is this needed if build-essentials is installed?
1. Ensure "deb-src" is enabled in source lists to install source packages
1. Obtain source be it a source package or from git repository
1. Make changes
1. Obtain deps sources using either `apt build-dep [package]` or manual install
1. Build `dpkg-buildpackage -rfakeroot -b -uc -us`
1. Test by installing `dpkg -i [package]`
1. Fix errors and rebuild


1. mkdir [package_ver-rev_arch] is there an option to specify ver-rev_arch so 
directory can just be [package]?
2. cd [package]
3. mkdir DEBIAN
4. touch DEBIAN/[{preinst,postinst,prerm,postrm}
5. chmod +x DEBIAN/[{preinst,postinst,prerm,postrm}
6. dpkg-deb --build --root-owner-group [sourcedir]
7. dpkg -i [package]

Obtain .deb information
  `dpkg-deb -I [package]`

## control

Architecture: amd64
Depends: libc6 (>= 2.29), libgcc-s1 (>= 3.3)
Homepage: https://neovim.io/
Maintainer: https://gitlab.com/techore/
Package: neovim
Priority: optional
Provides: editor
Recommends: python3-pynvim, xsel | xclip | wl-clipboard, xxd
Section: editors
Suggests: ctags, vim-scripts
Version: 0.9.0
Description: heavily refactored vim fork.
 Neovim is a fork of Vim focused on modern code and features, rather than
 running in legacy environments.

 ## .deb extract

READ THIS!!!
https://unix.stackexchange.com/questions/138188/easily-unpack-deb-edit-postinst-and-repack-deb

 dpkg-deb -R [package] [destdir]

 dpkg-deb -B [sourcedir] [package]

 Use package "fakeroot-ng" (deprec fakeroot) 

## repository

https://linuxconfig.org/easy-way-to-create-a-debian-package-and-local-package-repository

```
https://linuxconfig.org/easy-way-to-create-a-debian-package-and-local-package-repository
sudo mkdir /var/www/html/debian
sudo cp /path/to/linuxconfig-1.0_amd64.deb /var/www/html/debian/
dpkg-scanpackages . | gzip -c9  > Packages.gz
```

Update sources.lists

```
echo "deb [trusted=yes] http://10.1.1.4/debian ./" | tee -a /etc/apt/sources.list > /dev/null
```

## man page and license

https://www.youtube.com/watch?v=6KViyfAJQmY

## debmake

http://junyelee.blogspot.com/2021/06/build-debian-packages.html
https://github.com/BhanuKiranChaluvadi/debhello

## apt-cache

Print package info.

```
apt-cache depends neovim

```

## apt-rdepends

Provides recursive dependencies.

```
apt install apt-rdepends
```

```
apt-rdepends neovim
```

2023jun22

https://www.thetechedvocate.org/how-to-create-deb-packages-for-debian-ubuntu/

1. Install tools

sudo apt-get install dpkg debhelper devscripts

2. Create source directory

3. Create required files

* control - package meta data
* changelog - history of changes
* rules - instuction for building the package

4. Add files

Copy all the files for the packae including source code, data file, and any other fils to the source directory.

5. Build

debuild -us -uc

6. Test

sudo dpkg - package.deb


7. Distribute


https://ubuntuforums.org/showthread.php?t=910717

1. Naming convention: project_majver.minver-packagerev

example: fonts-fiction_1.0-1

2. Create directory

mkdir fonts-fiction_1.0-1

3. Create path using . as root

mkdir -p /usr/share/fonts/opentype/fiction

4. Create control

mkdir debian
nvim debian/control

Package: Fiction
Version: 1.0-1
Section: font
Priority: optional
Architecture: all
Depends: 
Maintainer: Your Name <you@email.com>
Description: Hello World
 When you need some sunshine, just run this
 small program!


 References:

 https://wiki.debian.org/Fonts/PackagingPolicy#Git_Repository
 https://salsa.debian.org/fonts-team
