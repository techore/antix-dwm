local opt = vim.opt

opt.tabstop = 4
opt.softtabstop = 4
opt.shiftwidth = 4
opt.shiftround = false
opt.expandtab = true
opt.autoindent = true
opt.smartindent = true
opt.hlsearch = false
opt.incsearch = true
opt.scrolloff = 8
opt.colorcolumn = "80"
opt.updatetime = 1000