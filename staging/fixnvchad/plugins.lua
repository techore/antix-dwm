--local overrides = require("custom.configs.overrides")

---@type NvPluginSpec[]
local plugins = {

-- Install a plugin
  {
    "vifm/vifm",
    event = "InsertEnter",
    config = function()
      require("vifm").setup()
    end,
  },

  {
    "vimwiki/vimwiki",
    event = "InsertEnter",
    config = function()
      require("vimwiki").setup()
    end,
  },

}

return plugins
