---@type ChadrcConfig
local M = {}

M.ui = {
    statusline = {
        theme = "vscode_colored", -- default/vscode/vscode_colored/minimal
        -- default/round/block/arrow separators work only for default statusline theme
        -- round and block will work for minimal theme only
        --separator_style = "block",
        --overriden_modules = nil,
    },
    theme = "nord",
    changed_themes = {
        nord = {
        base_16 = {
            base00 = "#1C1F27",
        },
        base_30 = {
            black = "#1C1F27",
        },
        },
    },
}

M.plugins = "custom.plugins"

return M
