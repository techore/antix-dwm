#!/bin/env bash
# Project: antix-dwm
# Location: /usr/local/src/antix-dwm/inst/inst-nemo.sh
# Dependency: gtk3
# Description: install nemo file manager.
# Usage: sudo inst-nemo.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "=========>  Updating apt cache\n\n"
    apt update
    printf "\n=========>  Installing nemo\n\n"
    apt --yes install nemo nemo-fileroller
    printf "\n=========>  nemo install complete!\n\n"
fi
