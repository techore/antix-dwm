# UEFI

1. Create partitions, `cfdisk /dev/sda`
2. Label type: gpt
3. "New" 256M partition, "Type" EFI System
4. "New" remainder, accept default type (Linux filesystem)
5. "Write" and type "yes"
6. "Quit"
7. Format sda1, `mkfs.fat -F 32 -n esp /dev/sda1`
8. Format sda2, `mkfs.ext4 -L antix /dev/sda2`

9. Upgrade packages, `apt update && apt -y upgrade`
10. Create mount point, `mkdir /media/sda2`
11. Mount root, `mount /dev/sda2 /media/sda2`

12. cli-installer will complete, unmount sda2, and exit
13. Mount root, `mount /dev/sda2 /media/sda2`
14. chroot using `chroot-rescue-scan` and select sda2
15. Mount esp, `mount /dev/sda1 /boot/efi`

6. Create a new fstab, `genfstab -U / > /etc/fstab`
7. Update `vim /etc/fstab` and remove extraneous entries, e.g. resolve.conf
8. Install grub2 for UEFI package, `apt install grub-efi`
9. Install grub to disk, `grub-install /dev/sda`
10. Generate grub configuration file, `update-grub`
11. Review UEFI entries, `efibootmgr`
12. Unmount esp, `umount /dev/sda1`
13. `exit` chroot then "q" and "q" to quit
14. `poweroff`
15. Remove USB
16. poweron and verify operation
