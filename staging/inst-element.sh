#!/usr/bin/env bash
# Location: install/inst-element.sh
# Dependency: gpg
# Description: Install riot (element) gpg key and repository.
#    Element is a matrix client.
# Usage: sudo inst-element.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "  Installing riot (element) gpg key and repository"
wget -qO - 'https://packages.element.io/debian/riot-im-archive-keyring.gpg' | gpg --dearmor | sudo tee /usr/share/keyrings/riot-im-archive-keyring.gpg 1> /dev/null
#    wget -O- https://packages.riot.im/debian/riot-im-archive-keyring.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/riot-im-archive-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/riot-im-archive-keyring.gpg] https://packages.riot.im/debian/ default main" | sudo tee /etc/apt/sources.list.d/riot-im.list
    printf "  Install element"
    sudo apt update
    sudo apt -y install riot-desktop
fi
