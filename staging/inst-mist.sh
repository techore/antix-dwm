#!/usr/bin/env bash
# Location: /usr/local/src/antix-dwm/install/inst-prebuilt-mpr.sh
# Dependency: gpg
# Description: Install mist and makedeb for installing prebuilt-mpr packages
#   and pulling sources from makedeb.org, respectively.
# Notes: 
#   See available packages at:
#       https://github.com/makedeb/prebuilt-mpr/blob/main/packages.txt 
#   makedeb, makedeb-beta, or makedeb-alpha are a dependency of mist
# Usage: sudo inst-prebuilt-mpr.sh

if test $EUID -gt 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    # Install dependency
    apt install gpg

    # Install gpg key and create apt source file for makedeb repository
#    printf "  Installing makedeb public key and source file."
#    wget -qO - 'https://proget.makedeb.org/debian-feeds/makedeb.pub' | gpg --dearmor | sudo tee /usr/share/keyrings/makedeb-archive-keyring.gpg 1> /dev/null
#    echo 'deb [signed-by=/usr/share/keyrings/makedeb-archive-keyring.gpg arch=all] https://proget.makedeb.org/ makedeb main' | sudo tee /etc/apt/sources.list.d/makedeb.list

    # Install gpg key and create apt source file for prebuilt-mpr repository
    printf "  Installing prebuilt-mpr public key and source file."
    wget -qO - 'https://proget.makedeb.org/debian-feeds/prebuilt-mpr.pub' | gpg --dearmor | sudo tee /usr/share/keyrings/prebuilt-mpr-archive-keyring.gpg 1> /dev/null
    echo "deb [arch=amd64,all signed-by=/usr/share/keyrings/prebuilt-mpr-archive-keyring.gpg] https://proget.makedeb.org prebuilt-mpr $(lsb_release -cs)" | sudo tee /etc/apt/sources.list.d/prebuilt-mpr.list

    # Update apt cache and install makedeb and mist
    printf "  Update apt cache and install makedeb and mist." 
    apt update
    apt install makedeb mist
fi
