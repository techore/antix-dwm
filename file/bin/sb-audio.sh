#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/sb-audio.sh
# Dependencies: amixer from alsa-tools package and yad-volume and \
#   alsa-set-default-card from antix-goodies package.
# Description: Display current volume using amixer. 
# Usage: Called by dwmblocks. 
#   Trigger is mouse buttons left (1), middle (2), and right (3) where
#   button+shift for 4, 5, and 6.

audio() {

case $BUTTON in
    1) yad-volume ;;
#    2) ;;
#    3) ;; 
    4) "$TERMINAL" -e alsa-set-default-card ;;
#    5) ;;
    6) "$TERMINAL" -e "$EDITOR" "$0" ;;
esac

}

audio

tog=$(amixer -M get Master | tail -n1 | sed -r 's/.*\[(o.*)\].*/\1/')
vol=$(amixer -M get Master | tail -n1 | sed -r 's/.*\[(.*)%\].*/\1/')

if [ "$tog" = "off" ]; then
    printf ""
else
    printf " %s%%" "$vol"
fi 
