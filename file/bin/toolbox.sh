!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/toolbox.sh
# Dependencies: rofi, rofi-toolmenu.sh, and defined programs.
# Description: rofi dmenu with a variety of tools.
# Usage:
#     Called using dwm keybinding mod+ctrl+p.

chosen=$(rofi-toolmenu.sh | rofi -dmenu -i -p Toolbox)

case "$chosen" in
    # System Tools
    "Date and Time") set_time-and_date.sh
        kill -36 $(pidof dwmblocks) ;;
    "Startup Services") gksu runit-service-manager.sh ;;
    "Alternatives Configuration") gksu galternatives ;;
    "Task Manager") gksu lxtask ;;
    "Screenshot") antixscreenshot.sh ;;
    "PC Information") inxi-gui ;;
    "antix-cli-cc") st -c toolbox -n toolbox -t toolbox antiX-cli-cc ;;
    "antiX Install") gksu minstall ;;

    # Hardware Tools
    "System Keyboard Layout") system-keyboard-qt ;;
    # techore 2022dec29: usefulness is in question. find an alternative?
#    "Mouse") ds-mouse ;;
    "Backlight Brightness") st -c toolbox -n toolbox -t toolbox backlight-brightness
        kill -42 $(pidof dwmblocks) ;;
    "Print Settings") gksu system-config-printer ;;
    # Disk Tools
    "Partition Editor") gksu gparted ;;
    # techore 2022dec21: selecting "apply" crashes xorg.
    # package: automount-antix
#    "Automount") automount-config ;;
    "Image Partition") st -c toolbox -n toolbox -t toolbox su-to-root -c partimage ;;
    "Synchronize Directories") bash -c 'setsid grsync' & ;;
    
    # Network Tools
    # package: cmst
    "cmst") cmst ;;
    "ceni") st -c toolbox -n toolbox -t toolbox su-to-root -c ceni ;;
    "Wifi switch") antix-wifi-switch ;;
    # techore 2022dec21: does not close and unable to test.
    # package: pppoeconf
#    "ADSL/PPPOE configuration") st -c toolbox -n toolbox -t toolbox gksu pppoeconf ;;
    # techore 2022dec29: no way to test and no value?
#    "Dial-up Configuration") gnome-ppp ;;
    # techore 2023jan07: doesn't work with ceni.. what are the dependencies? disabling for now.
    # package: wpagui
#    "WPA Supplicant") wpa_gui ;;
    "Block Adverts") gksu block-advert.sh ;;
    # techore 2022dec21: privilege credential wierdness. not working?
    # package: connectshares-antix
#    "Connect shares") st -c toolbox -n toolbox -t toolbox sudo connectshares-config.sh ;;
    "Droopy File Sharing") droopy.sh ;;
    # techore 2022dec21: previlege credential wierdness. not working?
    # package: connect-shares-antix
#    "disconnectshares.sh") st -c toolbox -n toolbox -t toolbox sudo disconnectshares.sh ;;
    
    # Sound Tools
    "ALSA Default Card") bash -c 'setsid alsa-set-default-card' & ;;
    "Speaker Test") st -c toolbox -n toolbox -t toolbox speaker-test -c 2 -t wav -l 3 ;;
    "ALSA Mixer") st -c toolbox -n toolbox -t toolbox alsamixer
        kill -41 $(pidof dwmblocks) ;;
    "ALSA Equalizer") st -c toolbox -n toolbox -t toolbox alsamixer -D equalizer ;;
    # Software Tools
    "Repo Manager") gksu repo-manager ;;
    "antiX Updater") gksu yad-updater
        upgrades=$(sudo apt-get upgrade --simulate --quiet=2 | grep Inst | wc -l)
        printf "$upgrades\n" > /var/tmp/aptupg.out
        kill -38 $(pidof dwmblocks) ;;
    "antiX Autoremove") gksu yad-autoremove ;;
    "Package Installer") gksu packageinstaller ;;
    "Synaptic Package Manager") gksu synaptic
        upgrades=$(sudo apt-get upgrade --simulate --quiet=2 | grep Inst | wc -l)
        printf "$upgrades\n" > /var/tmp/aptupg.out
        kill -38 $(pidof dwmblocks) ;;
    "cli-aptiX") st -c toolbox -n toolbox -t toolbox su-to-root -c cli-aptiX
        upgrades=$(sudo apt-get upgrade --simulate --quiet=2 | grep Inst | wc -l)
        printf "$upgrades\n" > /var/tmp/aptupg.out
        kill -38 $(pidof dwmblocks) ;;
    
    # Desktop Tools
    "ARandR Screen Layout Editor") arandr ;;
    "LXAppearance") lxappearance ;;
    "Qt5 Configuration Tool") qt5ct ;;
    "Kvantum Manager") kvantummanager ;;
    # techore 2022dec23: prefer my solution using .xinitrc.
#    "Wallpaper") wallpaper ;;
    # techore 2022dec21: selecting "apply" sets dpi in .Xresources then crashes xorg.
    # package: set-dpi-antix
#    "Font DPI") gksu set-dpi ;;
    # techore 2022dec23: prefer my solution using .xinitrc.
    # package: set-screen-blank-antix
#    "Screensaver") set-screen-blank ;;
#   # techore 2023jan07: not my cup of tea.
#   # package: desktop-defaults-antix
    #"Preferred Applications") desktop-defaults-set ;;
    
    # Live Tools
    "Configure Live Persistence") gksu persist-config ;;
    # techore 2022Dec21: persist-enable checks for two files, if found prints a message "Persist Root Enabled" and if not found exits. I am unclear how this is useful.
    # package: remaster-antix
#    "Enable live persistence") st -c toolbox -n toolbox -t toolbox su-to-root -c persist-enabled && sleep 3 ;;
    "Live Persistence Excludes") st -c toolbox -n toolbox -t toolbox su-to-root -c "$EDITOR /usr/local/share/excludes/persist-save-exclude.list" ;;
    "Live USB Maker") live-usb-maker-gui-antix ;;
    "Live USB Kernel Updater") st -c toolbox -n toolbox -t toolbox su-to-root -c live-kernel-updater ;;
    
    # Maintenance Tools
    "antiX User Manager") gksu antix-user ;;
    "ISO Snapshot") iso-snapshot ;;
    # techore 2022dec23: not my cup of tea.
    # package: luckybackup
#    "System Backup") luckybackup ;;
    "Network Assistance") network-assistant ;;
    "Boot Repair") bootrepair ;;
    
    # Monitoring Tools
    "Resource Monitor (btop)") st -c toolbox -n toolbox -t toolbox btop ;;
    "Process Viewer (htop)") st -c toolbox -n toolbox -t toolbox sudo htop ;;
    "Disk I/O Monitor (iotop)") st -c toolbox -n toolbox -t toolbox sudo iotop ;;
    "Bandwidth Usage (iftop)") st -c toolbox -n toolbox -t toolbox sudo iftop ;;
    *) exit ;;
esac
