#!/bin/env bash
# Project antix-dwm
# Location: /usr/local/bin/conf-1440p.sh
# Dependency:
# Description: Update respin installation files for hidpi 1440p support.
# Usage: sudo conf-1440p.sh

if test $EUID -gt 0; then

    # .Xresources
    printf "\n=========>  Update ~/.Xresources\n\n"
    sed -i 's/Xft.dpi: 96/Xft.dpi: 128/' ~/.Xresources

    sudo conf-1440p-root.sh

else
    printf "\n    Must be your user account not root!\n\n"
    exit 1
fi

printf "\n    Success!\n\n"
