#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/sb-battery.sh
# Dependencies: Material Design Icons
# Description: print current battery state
# Usage: Called by dwmblocks.

battery () {

PATH_AC="/sys/class/power_supply/AC"
PATH_BATTERY_0="/sys/class/power_supply/BAT0"

ac=0
battery_level_0=0
battery_max_0=0

if [ -f "$PATH_AC/online" ]; then
    ac=$(cat "$PATH_AC/online")
fi

if [ -f "$PATH_BATTERY_0/energy_now" ]; then
    battery_level_0=$(cat "$PATH_BATTERY_0/energy_now")
fi

if [ -f "$PATH_BATTERY_0/energy_full" ]; then
    battery_max_0=$(cat "$PATH_BATTERY_0/energy_full")
fi

battery_level=$battery_level_0
battery_max=$battery_max_0

battery_percent=$(("$battery_level * 100"))
battery_percent=$(("$battery_percent / $battery_max"))

if [ "$ac" -eq 1 ]; then
    icon=""

    if [ "$battery_percent" -gt 97 ]; then
        printf "$icon $battery_percent%%"
    else
	icon=""
        printf "$icon $battery_percent%%"
    fi
else
    if [ "$battery_percent" -gt 90 ]; then
        icon=""
    elif [ "$battery_percent" -gt 70 ]; then
        icon=""
    elif [ "$battery_percent" -gt 50 ]; then
        icon=""
    elif [ "$battery_percent" -gt 30 ]; then
        icon=""
    elif [ "$battery_percent" -gt 10 ]; then
        icon=""
    else
        icon=""
    fi

    printf "$icon $battery_percent%%"

fi
}

battery
