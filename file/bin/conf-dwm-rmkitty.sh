#!/bin/env bash
# Project: antix-dwm
# Location: /usr/local/src/antix-dwm/install/conf-dwm-rmkitty.sh
# Dependency:
# Description: Replace occurents of kitty with st.
# Usage: sudo ./conf-dwm-rmkitty.sh

# Test for root or sudo.
if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

# Set default if no argument was provided.
if test -z $1; then
    confdir="/usr/local/src/dwm-flexipatch"
else
    confdir=$1
fi

# Test directory.
if test ! -d "$confdir"; then
    printf "\n    Directory test failed!"
    printf "\n    Provided the correct path to configuration files.\n\n"
    exit 1
fi

# Verify config.def.h is present.
if test ! -f "$confdir/config.def.h"; then
    printf "\n    File test failed!"
    printf "\n    File config.def.h is missing.\n\n"
    exit 1
fi

# Tests complete!

# Backup config.h if exists.
if test -f "$confdir/config.h"; then
    cp "$confdir/config.h" "$confdir/config.h.bak"
fi

# sed: escape $.*/[\]^
# sed: tab = \t, newline = \n, use of \\ may be necessary

printf "    Updating config.h\n\n"
sed -i '{
    s/static const char \*termcmd\[\]  = { "kitty", NULL }/static const char \*termcmd\[\]  = { "st", NULL }/
    s/static const char \*filemanager\[\]  = { "kitty", "lf", NULL }/static const char \*filemanager\[\]  = { "st", "lf", NULL }/
    s/static const char \*scratchpadcmd\[\] = {"s", "kitty", "--name", "spterm", NULL}/static const char \*scratchpadcmd\[\] = {"s", "st", "-n", "spterm", NULL}/
}' "$confdir/config.h"

sed -i 's/RULE(.class = "kitty", .isterminal = 1)/RULE(.class = "st", .isterminal = 1)/' "$confdir/config.h"

# Compile and install binaries
make -C "$confdir" clean install

printf "\nSuccess!\n\n"

exit
