#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/sb-ipaddress.sh
# Dependencies:
# Description: display the current device and IP address.
# Usage: Called by dwmblocks.
# Reference: https://stackoverflow.com/questions/21336126/linux-bash-script-to-extract-ip-address

ipaddr () {
  case $BUTTON in
#    1) ;;
#    2) ;;
#    3) ;;
    4) su-to-root -X -c 'kitty --class "ceni" --name "ceni" --title "ceni" ceni' ;;
#    5) ;;
    6) "$TERMINAL" -e "$EDITOR" "$0" ;;
  esac
}

ipaddr

netdev=$(ip route get 8.8.8.8 | sed 's/.*dev \([^ ]*\).*/\1/;t;d')
netip=$(ip route get 8.8.8.8 | sed 's/.*src \([^ ]*\).*/\1/;t;d')

if test -z $netip; then
    printf "no: ip"
else
    printf "$netdev: $netip"
fi
