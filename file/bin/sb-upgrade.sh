#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/sb-upgrade.sh
# Dependencies: lxpolkit, libpolkit-agent-1-0,
# Description:
# mouse right click to launch execute sb-upgrade-updater
# mouse shift + right click to launch packageinstaller.
# Usage: Called by dwmblocks.
#   Trigger is mouse buttons left (1), middle (2), and right (3) where
#   button+shift for 4, 5, and 6.

file=/var/tmp/aptupg.out

upgrades () {
  case $BUTTON in
    1) bash -c sb-upgrade-updater.sh ;;
#    2) ;;
    3) gksu synaptic ;;
    4) su-to-root -X -c packageinstaller ;;
#    5) ;;
    6) "$TERMINAL" -e "$EDITOR" "$0" ;;
  esac
}

upgrades

if test -f "$file"; then
  pkgupdates=$(cat $file)
else
  pkgupdates="0"
fi

printf " %s" "$pkgupdates"
