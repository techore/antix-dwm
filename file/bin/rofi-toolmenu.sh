#!/usr/bin/env bash
# Usage: ./toolbox.sh | rofi -dmenu -i -p Toolbox

IFS=$'\n'

declare -a toolbox
toolbox=(
    "submenu,System Tools"
        "item,Date and Time"
        "item,Startup Services"
        "item,Task Manager"
        "item,Alternatives Configuration"
        "item,Screenshot"
        "item,PC Information"
        "item,antix-cli-cc"
        "item,antiX Install"
    "submenu,Hardware Tools"
        "item,System Keyboard Layout"
        "item,Backlight Brightness"
        "item,Print Settings"
    "submenu,Disk Tools"
        "item,Partition Editor"
        "item,Image Partition"
        "item,Synchronize Directories"
    "submenu,Network Tools"
        "item,cmst"
        "item,ceni"
        "item,Wifi switch"
        "item,Block Adverts"
        "item,Droopy File Sharing"
    "submenu,Sound Tools"
        "item,ALSA Default Card"
        "item,Speaker Test"
        "item,ALSA Mixer"
        "item,ALSA Equalizer"
    "submenu,Software Tools"
        "item,Repo Manager"
        "item,antiX Updater"
        "item,antiX Autoremove"
        "item,Package Installer"
        "item,Synaptic Package Manager"
        "item,cli-aptiX"
    "submenu,Desktop Tools"
        "item,ARandR Screen Layout Editor"
        "item,LXAppearance"
        "item,Qt5 Configuration Tool"
        "item,Kvantum Manager"
    "submenu,Live Tools"
        "item,Configure Live Persistence"
        "item,Live Persistence Excludes"
        "item,Live USB Maker"
        "item,Live USB Kernel Updater"
    "submenu,Maintenance Tools"
        "item,antiX User Manager"
        "item,ISO Snapshot"
        "item,Network Assistance"
        "item,Boot Repair"
    "submenu,Monitoring Tools"
        "item,Resource Monitor (btop)"
        "item,Process Viewer (htop)"
        "item,Disk I/O Monitor (iotop)"
        "item,Bandwidth Usage (iftop)"
)

for index in ${toolbox[*]} 
do
    IFS="," read -r -a tool <<< $index
    if test "${tool[0]}" == "submenu" ; then
        # Unable to figure out how to pass below options using printf :(
        echo -en "> ${tool[1]}\0nonselectable\x1ftrue\n"
    elif test "${tool[0]}" == "item" ; then
#        echo -en "${tool[1]}\n"
        printf '%s\n' "${tool[1]}"
    else
        exit
    fi
done
