#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/rofi-powermenu.sh
# Dependencies: rofi, slock, libyajl-dev package, dwm ipc patch
# Description: prompt user to select lock, quit, reboot, or poweroff
# Usage: called by sb-powermenu.sh when using power icon on statusbar or keybinding (default: mod+shift+p).

chosen=$(printf "lock\nreset\nquit\nreboot\npoweroff" | rofi -dmenu -i -theme-str '@import "power.rasi"')

case "$chosen" in
    # lock xorg sesssion
    "lock") slock ;;
    # reset dwm
    "reset") kill -HUP $(pidof dwm) ;;
    # quit dwm
    "quit") dwm-msg run_command quit ;;
    # reboot computer
    "reboot") sudo reboot ;;
    # poweroff computer
    "poweroff") sudo poweroff ;;
    *) exit ;;
esac
