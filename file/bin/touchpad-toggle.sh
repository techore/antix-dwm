#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/toggle-touchpad.sh
# Dependencies: xinput libnotify-bin dunst
# Description: Simple script to enable or disable touchpad and
#   send a desktop notification using libnotify.
# Usage: dwm calls on use of XF86XK_TouchpadToggle. If your laptop
#   does not have a touchpad toggle key, assign to a key combo
#   using dwm's config.h file.

TOUCHPAD=`xinput list | grep -Eio '(touchpad|glidepoint)\s*id\=[0-9]{1,2}'`

if test "$TOUCHPAD" = "" ; then
    notify-send "Touchpad undetected" -t 3000
    exit
fi

ID=`grep -Eo '[0-9]{1,2}' <<< "$TOUCHPAD"`

STATE=`xinput list-props $ID|grep 'Device Enabled'|awk '{print $4}'`

if test $STATE -eq 0 ; then
    xinput enable $ID
    notify-send "Touchpad enabled" -t 3000    
else test $STATE -eq 1 
    xinput disable $ID
    notify-send "Touchpad disabled" -t 3000
fi
