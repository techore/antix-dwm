#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/rofi-progmenu.sh
# Dependencies: rofi and defined programs.
# Description: prompt user to select defined programs
# Usage:
#     Called using dwm keybinding mod+p.
#     Edit for your preferred programs. rofi theme programs.rasi theme may need to be udpated, e.g. lines. 

chosen=$(printf "dwm\nst\nkitty\nkitty 2\nfish\nfish 2\nvifm\nnvim\nnvim 2\nvimwiki\nvimwiki 2\ntmux\ntmux 2\ntmux 3\ndisable" | rofi -dmenu -i -theme-str '@import "cheatsheet.rasi"')

case "$chosen" in
    "dwm")      pkill conky
                conky -c /etc/conky/conky-dwm.conf ;;
    "st")       pkill conky
                conky -c /etc/conky/conky-st.conf ;;
    "kitty")  pkill conky
                conky -c /etc/conky/conky-kitty1.conf ;;
    "kitty 2")  pkill conky
                conky -c /etc/conky/conky-kitty2.conf ;;
    "fish")  pkill conky
                conky -c /etc/conky/conky-fish1.conf ;;
    "fish 2")  pkill conky
                conky -c /etc/conky/conky-fish2.conf ;;
    "vifm")     pkill conky
                conky -c /etc/conky/conky-vifm.conf ;;
    "nvim")     pkill conky
                conky -c /etc/conky/conky-nvim1.conf ;;
    "nvim 2")   pkill conky
                conky -c /etc/conky/conky-nvim2.conf ;;
    "vimwiki")  pkill conky
                conky -c /etc/conky/conky-vimwiki1.conf ;;
    "vimwiki 2") pkill conky
                conky -c /etc/conky//conky-vimwiki2.conf ;;
    "tmux")     pkill conky
                conky -c /etc/conky/conky-tmux1.conf ;;
    "tmux 2")   pkill conky
                conky -c /etc/conky/conky-tmux2.conf ;;
    "tmux 3")   pkill conky
                conky -c /etc/conky/conky-tmux3.conf ;;
    "disable")  pkill conky ;;
    *) exit ;;
esac
