#!/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/conf-slock.sh
# Dependencies:
# Description: To avoid maintaining and reconciling a fork, this script updates
# configuration files then compiles and installs binaries.
# Usage: `sudo conf-slock.sh path` where path is the directory for patches.def.h and
# config.def.h.


# Test for root or sudo.
if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

# Set defautl if no argument was provided.
if test -z $1; then
    confdir="/usr/local/src/dwmblocks"
else
    confdir=$1
fi

# Test directory.
if test ! -d "$confdir"; then
    printf "\n    Directory test failed!"
    printf "\n    Provided the correct path to configuration files.\n\n"
    exit 1
fi

# Verify patches.def.h is present.
if test ! -f "$confdir/patches.def.h"; then
    printf "\n    File test failed!"
    printf "\n    File patches.def.h is missing.\n\n"
    exit 1
fi

# Verify config.def.h is present.
if test ! -f "$confdir/config.def.h"; then
    printf "\n    File test failed!"
    printf "\n    File config.def.h is missing.\n\n"
    exit 1
fi

# Tests complete!

# Backup patches.h if exists then copy patches.def.h to patches.h.
if test -f "$confdir/patches.h"; then
    cp "$confdir/patches.h" "$confdir/patches.h.bak"
    cp "$confdir/patches.def.h" "$confdir/patches.h"
    else
        cp "$confdir/patches.def.h" "$confdir/patches.h"
fi

# Backup config.h if exists then copy config.def.h to config.h.
if test -f "$confdir/config.h"; then
    cp "$confdir/config.h" "$confdir/config.h.bak"
    cp "$confdir/config.def.h" "$confdir/config.h"
    else
        cp "$confdir/config.def.h" "$confdir/config.h"
fi

# sed: escape $.*/[\]^
# sed: tab = \t, newline = \n, use of \\ may be necessary

# Configure patches.h
sed -i '{
    s/#define CAPSCOLOR_PATCH 0/#define CAPSCOLOR_PATCH 1/
    s/#define DWM_LOGO_PATCH 0/#define DWM_LOGO_PATCH 1/
    s/#define QUICKCANCEL_PATCH 0/#define QUICKCANCEL_PATCH 1/
}' "$confdir/patches.h"

# Configure config.h
sed -i '{
    s/\[INPUT\] =  "#005577"/\[INPUT\] =  "#5E81AC"/
    s/\[FAILED\] = "#CC3333"/\[FAILED\] = "red"/
    s/\[CAPS\] =   "red"/\[CAPS\] =   "#EBCB8B"/
    s/static const int timetocancel = 4/static const int timetocancel = 15/
}' "$confdir/config.h"

# Compile and install binaries
make -C "$confdir" clean install

printf "\nSuccess!\n\n"

exit
