#!/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/conf-2160p.sh
# Dependency:
# Description: update respin installation files for 2160p support
# Usage: sudo conf-2160p.sh

if test $EUID -gt 0; then

    # .Xresources
    printf "\n=========>  Updating ~/.Xresources\n\n"
    sed -i 's/Xft.dpi: 96/Xft.dpi: 192/' ~/.Xresources

    sudo conf-2160p-root.sh

else
    printf "\n    Must be your user account not root!\n\n"
    exit 1
fi

printf "\n    Success!\n\n"
