#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/sb-powermenu.sh
# Dependencies: rofi and rofi-powermenu.sh
# Description: Click to execute /usr/local/bin/rofi-powermenu.sh
# Usage: Called by dwmblocks. 
#   Trigger is mouse buttons left (1), middle (2), and right (3) where
#   button+shift for 4, 5, and 6.

powerm () {

    case $BUTTON in
        1) rofi-powermenu.sh ;;
#        2) ;;
#        3) ;;
#        4) ;;
#        5) ;;
        6) "$TERMINAL" -e "$EDITOR" "$0" ;;
    esac

    printf " "

}

powerm
