#!//usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/wintitleinfo.sh
# Dependencies: xdotool, dunst, libnotify-bin (notify-send) packages 
# Description: display using notify-send window information to dunst for use
#   with a window manager to define window behavior.
# Usage: Called by dwmblocks.

title="$(xdotool getactivewindow getwindowname)"
windowresourceid="$(xprop -root | grep _NET_ACTIVE_WINDOW\(WINDOW\) | cut -d \# -f 2)"
windowclass="$(xprop -id $windowresourceid | grep -i class | cut -d = -f 2)"
windowpid="$(xdotool getactivewindow getwindowpid)"

notify-send "Title: $title" "Class: $windowclass\nPID: $windowpid"
