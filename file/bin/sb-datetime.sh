#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/sb-datetime.sh
# Dependencies: Material Design Icons
# Description: print to dwmblocks current day of week, date and hour:minute.
#   On left mouse click execute yad-calendar and on shift left mouse click
#   execute set_time-and_date.sh.
# Usage: Called by dwmblocks.
#   Trigger is mouse buttons left (1), middle (2), and right (3) where
#   button+shift for 4, 5, and 6.

datetime () {

case $BUTTON in
    1) yad-calendar ;;
#    2) ;;
#    3) ;;
    4) su-to-root -X -c set_time-and_date.sh ;;
#    5) ;;
    6) "$TERMINAL" -e "$EDITOR" "$0" ;;
esac

    printf " %s" "$(date "+%A %d | %H:%M")"
}

datetime
