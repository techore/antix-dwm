#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/sb-weather.sh
# Package dependencies:
#   curl and jq packages
#   Obtain $APPID from https://openweathermap.org
# Description: download and print weather info from openweathermap.org to statusbar.
# Usage: Called by dwmblocks.
#   Trigger is mouse buttons left (1), middle (2), and right (3) where
#   button+shift for 4, 5, and 6.

APIURL="http://api.openweathermap.org/data/2.5/weather"
LOCATION="McAlester,us"
UNITS="imperial"
APPID=""

currtemp=$(curl -sf "$APIURL?q=$LOCATION&units=$UNITS&APPID=$APPID" | jq '.main.temp')

weather () {

    case $BUTTON in
        1) setsid -f "$TERMINAL" -e wttr_in.sh ;;
#        2) ;;
#        3) ;; 
#        4) ;;
#        5) ;;
        6) "$TERMINAL" -e "$EDITOR" "$0" ;;
    esac

    printf "$(printf ' %.0f' $currtemp)°F"

}

weather
