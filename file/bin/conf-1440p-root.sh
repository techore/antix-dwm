#!/bin/env bash
# Project antix-dwm
# Location: /usr/local/bin/conf-1440p-root.sh
# Dependency:
# Description: Update respin installation files for hidpi 1440p support.
# Usage: Called from conf-1440p.sh

if test $EUID -eq 0 ; then

    # conf-dwm.sh
    if test -d /usr/local/src/dwm-flexipatch ; then
        printf "\n=========>  Updating conf-dwm.sh\n\n"
        sed -i '{
            s/static const unsigned int borderpx       = 4/static const unsigned int borderpx       = 6/
            s/static const unsigned int gappih         = 10/static const unsigned int gappih         = 14/
            s/static const unsigned int gappiv         = 10/static const unsigned int gappiv         = 14/
            s/static const unsigned int gappoh         = 10/static const unsigned int gappoh         = 14/
            s/static const unsigned int gappiv         = 99/static const unsigned int gappiv         = 10/
            s/static const unsigned int gappoh         = 99/static const unsigned int gappoh         = 10/
            s/static const unsigned int gappov         = 10/static const unsigned int gappov         = 14/
        }' /usr/local/bin/conf-dwm.sh

        # make dwm-flexipatch
        /usr/local/bin/conf-dwm.sh /usr/local/src/dwm-flexipatch
    else
        printf '\n%s\n\n' "=========>  /usr/local/src/dwm-flexipatch check failed!"
        exit
    fi

    # nord.rasi
    printf "\n=========>  Updating nord.rasi\n\n"
    sed -i 's/font: "Source Code Pro Regular 12/font: "Source Code Pro Regular 16/' /usr/share/rofi/themes/nord.rasi
    sed -i 's/border:       4/border:       6/' /usr/share/rofi/themes/nord.rasi

    # power.rasi
    printf "\n=========>  Updating power.rasi\n\n"
    sed -i 's/font: "Source Code Pro Regular 18/font: "Source Code Pro Regular 22/' /usr/share/rofi/themes/power.rasi

    # favorites.rasi
    printf "\n=========>  Updating favorites.rasi\n\n"
    sed -i 's/font: "Source Code Pro Regular 18/font: "Source Code Pro Regular 22/' /usr/share/rofi/themes/favorites.rasi

    # cheatsheet.rasi
    printf "\n=========>  Updating cheatsheet.rasi\n\n"
    sed -i 's/font: "Source Code Pro Regular 18/font: "Source Code Pro Regular 22/' /usr/share/rofi/themes/cheatsheet.rasi
    
    # conky.conf
    printf "\n=========>  Updating conky cheatsheets\n\n"
    for file in /etc/conky/conky-*.conf
    do
        sed -i '{
            s/gap_x = 20/gap_x = 28/
	        s/gap_y = 30/gap_y = 44/
	        s/minimum_width = 400/minimum_width = 520/
        }' "$file"
    done 
else
    printf "\n    Must be root or sudo!\n\n"
    exit 1
fi
