#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/rofi-favmenu.sh
# Dependencies: rofi and defined programs.
# Description: prompt user to select defined programs
# Usage:
#     Called using dwm keybinding mod+p.
#     Edit for your preferred programs. rofi theme programs.rasi theme may need to be udpated, e.g. lines. 

chosen=$(printf "text editor\nfile manager\ninternet browser\nyoutube fzf\nkeepassxc" | rofi -dmenu -i -theme-str '@import "favorites.rasi"')

case "$chosen" in
    "text editor") $TERMINAL -e nvim ;;
    "file manager") $TERMINAL -e vifm;;
    "internet browser") xdg-open "https://duckduckgo.com" ;;
    "youtube fzf") kitty --title youtube-fzf ytfzf -l -t -T kitty ;;
    "keepassxc") keepassxc ;;
    *) exit ;;
esac
