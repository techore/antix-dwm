#!/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/conf-2160p-root.sh
# Dependency:
# Description: update respin installation files for 2160p support
# Usage: Called from conf-2160p.sh

if test $EUID -eq 0; then

    # antixdwmenv.sh
    printf "=========>  Updating axdwm-envs.sh\n\n"
    sed -i '{
        s/export GDK_SCALE="1"/export GDK_SCALE="2"/
        s/export GDK_DPI_SCALE="1.0"/export GDK_DPI_SCALE="0.5"/
    }' /etc/profile.d/axdwm-envs.sh

    # confdwm.sh
    if test -d /usr/local/src/dwm-flexipatch ; then
        printf "\n=========>  Updating conf-dwm.sh\n\n"
        sed -i '{
            s/static const unsigned int borderpx       = 4/static const unsigned int borderpx       = 8/
            s/static const unsigned int gappih         = 10/static const unsigned int gappih         = 20/
            s/static const unsigned int gappiv         = 10/static const unsigned int gappiv         = 20/
            s/static const unsigned int gappoh         = 10/static const unsigned int gappoh         = 20/
            s/static const unsigned int gappiv         = 99/static const unsigned int gappiv         = 10/
            s/static const unsigned int gappoh         = 99/static const unsigned int gappoh         = 10/
            s/static const unsigned int gappov         = 10/static const unsigned int gappov         = 20/
        }' /usr/local/bin/conf-dwm.sh

        # make dwm-flexipatch
        /usr/local/bin/conf-dwm.sh /usr/local/src/dwm-flexipatch
    else
        printf '\n%s\n\n' "=========>  /usr/local/src/dwm-flexipatch check failed!"
        exit
    fi

    # nord.rasi
    printf "=========>  Updating nord.rasi\n\n"
    sed -i 's/font: "Source Code Pro Regular 12/font: "Source Code Pro Regular 24/' /usr/share/rofi/themes/nord.rasi
    sed -i 's/border:       4/border:       8/' /usr/share/rofi/themes/nord.rasi

    # power.rasi
    printf "=========>  Updating power.rasi\n\n"
    sed -i 's/font: "Source Code Pro Regular 18/font: "Source Code Pro Regular 36/' /usr/share/rofi/themes/power.rasi

    # favorites.rasi
    printf "=========>  Updating favorites.rasi\n\n"
    sed -i 's/font: "Source Code Pro Regular 18/font: "Source Code Pro Regular 36/' /usr/share/rofi/themes/favorites.rasi
    
    # cheatsheet.rasi
    printf "=========>  Updating cheatsheet.rasi\n\n"
    sed -i 's/font: "Source Code Pro Regular 18/font: "Source Code Pro Regular 36/' /usr/share/rofi/themes/cheatsheet.rasi

    # conky.conf
    printf "=========>  Updating conky.conf\n\n"
    for file in /etc/conky/conky-*.conf
    do
	  sed -i '{
        s/gap_x = 20/gap_x = 40/
	      s/gap_y = 30/gap_y = 60/
	      s/minimum_width = 400/minimum_width = 800/
    }' "$file"
    done
else
    printf "\n    Must be root or sudo!\n\n"
    exit 1
fi
