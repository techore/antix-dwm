#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/sb-brightness.sh
# Dependencies: account is a member of video group
# Description: display the current backlight brightness percent. Also,
# this script will be called by dwm-brightness.sh everytime XF86MonBrightnessDown
# and XF86MonBrightnessUp is detected to update dwmblocks.
# Usage: Called by dwmblocks.

bright () {
  case $BUTTON in
    1) kitty --class "backlight" --name "backlight" --title "backlight" backlight-brightness ;;
#    2) ;;
#    3) ;;
#    4) ;;
#    5) ;;
    6) "$TERMINAL" -e "$EDITOR" "$0" ;;
  esac
}

bright

printf " $(backlight-brightness --get)%%"
