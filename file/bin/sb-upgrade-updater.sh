#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/sb-upgrade-updater.sh
# Dependencies: yad-updater from antix-goodies package
# Description: Update packages then statusbar.
# Usage: Called by sb-upgrade.sh

# Update installed packages.
su-to-root -X -c yad-updater

# Update /var/tmp/aptupg.out
upgrades=$(sudo apt-get upgrade --simulate --quiet=2 | grep Inst | wc -l)
printf "$upgrades\n" > /var/tmp/aptupg.out

# Refresh upgrades on the statusbar
kill -38 $(pidof dwmblocks)
