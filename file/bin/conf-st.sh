#!/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/conf-st.sh
# Dependencies:
# Description: To avoid maintaining and reconciling a fork, this script updates
# configuration files then compiles and installs binaries.
# Usage: `sudo conf-st.sh path` where path is the directory for patches.def.h and
# config.def.h.


# Test for root or sudo.
if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

# Set default if no argument was provided.
if test -z $1; then
    confdir="/usr/local/src/st-flexipatch"
else
    confdir=$1
fi

# Test directory.
if test ! -d "$confdir"; then
    printf "\n    Directory test failed!"
    printf "\n    Provided the correct path to configuration files.\n\n"
    exit 1
fi

# Verify config.mk is present.
if test ! -f "$confdir/config.mk"; then
    printf "\n    File test failed!"
    printf "\n    File config.mk is missing.\n\n"
    exit 1
fi

# Verify patches.def.h is present.
if test ! -f "$confdir/patches.def.h"; then
    printf "\n    File test failed!"
    printf "\n    File patches.def.h is missing.\n\n"
    exit 1
fi

# Verify config.def.h is present.
if test ! -f "$confdir/config.def.h"; then
    printf "\n    File test failed!"
    printf "\n    File config.def.h is missing.\n\n"
    exit 1
fi

# Tests complete!

# Backup config.mk if exists.
if test -f "$confdir/config.mk"; then
    cp "$confdir/config.mk" "$confdir/config.mk.bak"
fi

# Backup patches.h if exists then copy patches.def.h to patches.h.
if test -f "$confdir/patches.h"; then
    cp "$confdir/patches.h" "$confdir/patches.h.bak"
    cp "$confdir/patches.def.h" "$confdir/patches.h"
    else
        cp "$confdir/patches.def.h" "$confdir/patches.h"
fi

# Backup config.h if exists then copy config.def.h to config.h.
if test -f "$confdir/config.h"; then
    cp "$confdir/config.h" "$confdir/config.h.bak"
    cp "$confdir/config.def.h" "$confdir/config.h"
    else
        cp "$confdir/config.def.h" "$confdir/config.h"
fi

# sed: escape $.*/[\]^
# sed: tab = \t, newline = \n, use of \\ may be necessary

# Configure config.mk
#sed -i '{
#    s/text1/text2/
#    s/text3/text4/
#}' "$confdir/config.mk"

# Configure patches.h
sed -i '{
    s/#define FONT2_PATCH 0/#define FONT2_PATCH 1/
    s/#define SCROLLBACK_PATCH 0/#define SCROLLBACK_PATCH 1/
    s/#define USE_XFTFONTMATCH_PATCH 0/#define USE_XFTFONTMATCH_PATCH 1/
    s/#define XRESOURCES_PATCH 0/#define XRESOURCES_PATCH 1/
}' "$confdir/patches.h"

# Configure config.h
sed -i '{
    s/static unsigned int cols = 80/static unsigned int cols = 120/
    s/static unsigned int rows = 24/static unsigned int rows = 40/
    s/static char \*font = "Liberation Mono:pixelsize=12:antialias=true:autohint=true"/static char \*font = "Source Code Pro:size=12:antialias=true:autohint=true"/
    s/\/\*	"Inconsolata for Powerline:pixelsize=12:antialias=true:autohint=true", \*\//"Symbola:size=12:antialias=true:autohint=true"/
}' "$confdir/config.h"

## keybindings
#sed -i '/static const char \*termcmd\[\]  = { "kitty", NULL };/a static const char \*filemanager\[\]  = { "kitty", "lf", NULL };' "$confdir/config.h"

# Compile and install binaries
make -C "$confdir" clean install

printf "\nSuccess!\n\n"

exit
