#!/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/conf-dwmblocks.sh
# Dependencies:
# Description: To avoid maintaining and reconciling a fork, this script updates
# configuration files then compiles and installs binaries.
# Usage: `sudo conf-dwmblocks.sh path` where path is the directory for blocks.def.h.


# Test for root or sudo.
if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

# Set default if no argument was provided.
if test -z $1; then
    confdir="/usr/local/src/dwmblocks"
else
    confdir=$1
fi

# Test directory.
if test ! -d "$confdir"; then
    printf "\n    Directory test failed!"
    printf "\n    Provided the correct path to dwmblocks.\n\n"
    exit 1
fi

# Verify blocks.def.h is present.
if test ! -f "$confdir/blocks.def.h"; then
    printf "\n    File test failed!"
    printf "\n    File blocks.def.h is missing.\n\n"
    exit 1
fi

# Tests complete!

# Backup blocks.h if exists then copy blocks.def.h to blocks.h.
if test -f "$confdir/blocks.h"; then
    cp "$confdir/blocks.h" "$confdir/blocks.h.bak"
    cp "$confdir/blocks.def.h" "$confdir/blocks.h"
    else
        cp "$confdir/blocks.def.h" "$confdir/blocks.h"
fi

# sed: escape $.*/[\]^
# sed: tab = \t, newline = \n, use of \\ may be necessary

# Delete empty lines and default entries
sed -i '{
    /^$/d
    /"Mem:", "free/d
    /"", "date/d
}' "$confdir/blocks.h"

# Add empty line; cosmetic only
sed -i 's/};/};\n/' "$confdir/blocks.h"

# Create blocks array.
sed -i '/\/\*Icon\*\//a \\/\/\t{"",\t\t"sb-battery.sh",\t15,\t\t\t9},\n\/\/\t{"",\t\t"sb-brightness.sh",\t0,\t\t\t8},\n\t{"",\t\t"sb-audio.sh",\t\t0,\t\t\t7},\n\t{"",\t\t"sb-weather.sh",\t1800,\t\t\t6},\n\t{"",\t\t"sb-airquality.sh",\t1800,\t\t\t5},\n\t{"",\t\t"sb-upgrade.sh",\t1800,\t\t\t4},\n\t{"",\t\t"sb-ipaddress.sh",\t30,\t\t\t3},\n\t{"",\t\t"sb-datetime.sh",\t15,\t\t\t2},\n\t{"",\t\t"sb-powermenu.sh",\t0,\t\t\t1},' "$confdir/blocks.h"

# Compile and install binaries
make -C "$confdir" clean install

printf "\nSuccess!\n\n"

exit
