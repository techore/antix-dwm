#!/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/conf-dwm.sh
# Dependencies:
# Description: To avoid maintaining and reconciling a fork, this script updates
# configuration files then compiles and installs binaries.
# Usage: `sudo conf-dwm.sh path` where path is the directory for patches.def.h and
# config.def.h.


# Test for root or sudo.
if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

# Set default if no argument was provided.
if test -z $1; then
    confdir="/usr/local/src/dwm-flexipatch"
else
    confdir=$1
fi

# Test directory.
if test ! -d "$confdir"; then
    printf "\n    Directory test failed!"
    printf "\n    Provided the correct path to configuration files.\n\n"
    exit 1
fi

# Verify config.mk is present.
if test ! -f "$confdir/config.mk"; then
    printf "\n    File test failed!"
    printf "\n    File config.mk is missing.\n\n"
    exit 1
fi

# Verify patches.def.h is present.
if test ! -f "$confdir/patches.def.h"; then
    printf "\n    File test failed!"
    printf "\n    File patches.def.h is missing.\n\n"
    exit 1
fi

# Verify config.def.h is present.
if test ! -f "$confdir/config.def.h"; then
    printf "\n    File test failed!"
    printf "\n    File config.def.h is missing.\n\n"
    exit 1
fi

# Tests complete!

# Backup config.mk if exists.
if test -f "$confdir/config.mk"; then
    cp "$confdir/config.mk" "$confdir/config.mk.bak"
fi

# Backup patches.h if exists then copy patches.def.h to patches.h.
if test -f "$confdir/patches.h"; then
    cp "$confdir/patches.h" "$confdir/patches.h.bak"
    cp "$confdir/patches.def.h" "$confdir/patches.h"
    else
        cp "$confdir/patches.def.h" "$confdir/patches.h"
fi

# Backup config.h if exists then copy config.def.h to config.h.
if test -f "$confdir/config.h"; then
    cp "$confdir/config.h" "$confdir/config.h.bak"
    cp "$confdir/config.def.h" "$confdir/config.h"
    else
        cp "$confdir/config.def.h" "$confdir/config.h"
fi

# sed: escape $.*/[\]^
# sed: tab = \t, newline = \n, use of \\ may be necessary

# Configure config.mk
sed -i '{
    s/#YAJLLIBS = -lyajl/YAJLLIBS = -lyajl/
    s/#YAJLINC = -I\/usr\/include\/yajl/YAJLINC = -I\/usr\/include\/yajl/
    s/#XCBLIBS = -lX11-xcb -lxcb -lxcb-res/XCBLIBS = -lX11-xcb -lxcb -lxcb-res/
}' "$confdir/config.mk"

# Configure patches.h
sed -i '{
    s/#define BAR_AWESOMEBAR_PATCH 0/#define BAR_AWESOMEBAR_PATCH 1/
    s/#define COOL_AUTOSTART_PATCH 0/#define COOL_AUTOSTART_PATCH 1/
    s/#define IPC_PATCH 0/#define IPC_PATCH 1/
    s/#define CYCLELAYOUTS_PATCH 0/#define CYCLELAYOUTS_PATCH 1/
    s/#define BAR_DWMBLOCKS_PATCH 0/#define BAR_DWMBLOCKS_PATCH 1/
    s/#define CENTER_PATCH 0/#define CENTER_PATCH 1/
    s/#define ATTACHBELOW_PATCH 0/#define ATTACHBELOW_PATCH 1/
    s/#define MOVESTACK_PATCH 0/#define MOVESTACK_PATCH 1/
    s/#define NOBORDER_PATCH 0/#define NOBORDER_PATCH 1/
    s/#define RESTARTSIG_PATCH 0/#define RESTARTSIG_PATCH 1/
    s/#define SAVEFLOATS_PATCH 0/#define SAVEFLOATS_PATCH 1/
    s/#define SEAMLESS_RESTART_PATCH 0/#define SEAMLESS_RESTART_PATCH 1/
    s/#define RENAMED_SCRATCHPADS_PATCH 0/#define RENAMED_SCRATCHPADS_PATCH 1/
    s/#define RENAMED_SCRATCHPADS_AUTO_HIDE_PATCH 0/#define RENAMED_SCRATCHPADS_AUTO_HIDE_PATCH 1/
    s/#define BAR_STATUSCMD_PATCH 0/#define BAR_STATUSCMD_PATCH 1/
    s/#define VANITYGAPS_PATCH 0/#define VANITYGAPS_PATCH 1/
    s/#define FOCUSONCLICK_PATCH 0/#define FOCUSONCLICK_PATCH 1/
    s/#define SWALLOW_PATCH 0/#define SWALLOW_PATCH 1/
    s/#define TOGGLEFULLSCREEN_PATCH 0/#define TOGGLEFULLSCREEN_PATCH 1/
    s/#define TOGGLELAYOUT_PATCH 0/#define TOGGLELAYOUT_PATCH 1/
    s/#define LOSEFULLSCREEN_PATCH 0/#define LOSEFULLSCREEN_PATCH 1/
}' "$confdir/patches.h"

# Configure config.h
sed -i '{
    s/#define MODKEY Mod1Mask/#define MODKEY Mod4Mask/
    s/static const char \*termcmd\[\]  = { "st", NULL }/static const char \*termcmd\[\]  = { "kitty", NULL }/
    s/static const unsigned int borderpx       = 1/static const unsigned int borderpx       = 4/
    s/static const unsigned int gappih         = 20/static const unsigned int gappih         = 10/
    s/static const unsigned int gappiv         = 99/static const unsigned int gappiv         = 10/
    s/static const unsigned int gappoh         = 99/static const unsigned int gappoh         = 10/
    s/static const unsigned int gappov         = 30/static const unsigned int gappov         = 10/
    s/static const int smartgaps_fact          = 1/static const int smartgaps_fact          = 0/
    s/static const char \*fonts\[\]               = { "monospace:size=10" }/static const char \*fonts\[\]               = { "Source Sans 3:size=12", "Material Icons Outlined:size=12:antialias=true:autohint=true" }/
    s/static const char \*scratchpadcmd\[\] = {"s", "st", "-n", "spterm", NULL}/static const char \*scratchpadcmd\[\] = {"s", "kitty", "--name", "spterm", NULL}/
    s/RULE(.instance = "spterm", .tags = SPTAG(0), .isfloating = 1)/RULE(.instance = "spterm", .tags = SPTAG(0), .isfloating = 1, .iscentered = 1)/
    s/	{ MODKEY|Mod4Mask,              XK_0,          togglegaps,             {0} },/	{ MODKEY|Mod4Mask,              XK_g,          togglegaps,             {0} },/
    s/	{ MODKEY|Mod4Mask|ShiftMask,    XK_0,          defaultgaps,            {0} },/ 	{ MODKEY|Mod4Mask|ShiftMask,    XK_g,          defaultgaps,            {0} },/
    s/static char normfgcolor\[\]                = "#bbbbbb"/static char normfgcolor\[\]                = "#d8dee9"/
    s/static char normbgcolor\[\]                = "#222222"/static char normbgcolor\[\]                = "#2e3440"/
    s/static char normbordercolor\[\]            = "#444444"/static char normbordercolor\[\]            = "#4c566a"/
    s/static char normfloatcolor\[\]             = "#db8fd9"/static char normfloatcolor\[\]             = "#4c566a"/
    s/static char selfgcolor\[\]                 = "#eeeeee"/static char selfgcolor\[\]                 = "#2e3440"/
    s/static char selbgcolor\[\]                 = "#005577"/static char selbgcolor\[\]                 = "#ebcb8b"/
    s/static char selbordercolor\[\]             = "#005577"/static char selbordercolor\[\]             = "#5e81ac"/
    s/static char selfloatcolor\[\]              = "#005577"/static char selfloatcolor\[\]              = "#5e81ac"/
    s/static char titlenormfgcolor\[\]           = "#bbbbbb"/static char titlenormfgcolor\[\]           = "#d8deed"/
    s/static char titlenormbgcolor\[\]           = "#222222"/static char titlenormbgcolor\[\]           = "#2e3440"/
    s/static char titlenormbordercolor\[\]       = "#444444"/static char titlenormbordercolor\[\]       = "#2e3440"/
    s/static char titlenormfloatcolor\[\]        = "#db8fd9"/static char titlenormfloatcolor\[\]        = "#2e3440"/
    s/static char titleselfgcolor\[\]            = "#eeeeee"/static char titleselfgcolor\[\]            = "#e5e9f0"/
    s/static char titleselbgcolor\[\]            = "#005577"/static char titleselbgcolor\[\]            = "#5e81ac"/
    s/static char titleselbordercolor\[\]        = "#005577"/static char titleselbordercolor\[\]        = "#5e81ac"/
    s/static char titleselfloatcolor\[\]         = "#005577"/static char titleselfloatcolor\[\]         = "#5e81ac"/
    s/static char tagsnormfgcolor\[\]            = "#bbbbbb"/static char tagsnormfgcolor\[\]            = "#d8deed"/
    s/static char tagsnormbgcolor\[\]            = "#222222"/static char tagsnormbgcolor\[\]            = "#2e3440"/
    s/static char tagsnormbordercolor\[\]        = "#444444"/static char tagsnormbordercolor\[\]        = "#2e3440"/
    s/static char tagsnormfloatcolor\[\]         = "#db8fd9"/static char tagsnormfloatcolor\[\]         = "#2e3440"/
    s/static char tagsselfgcolor\[\]             = "#eeeeee"/static char tagsselfgcolor\[\]             = "#e5e9f0"/
    s/static char tagsselbgcolor\[\]             = "#005577"/static char tagsselbgcolor\[\]             = "#5e81ac"/
    s/static char tagsselbordercolor\[\]         = "#005577"/static char tagsselbordercolor\[\]         = "#5e81ac"/
    s/static char tagsselfloatcolor\[\]          = "#005577"/static char tagsselfloatcolor\[\]          = "#5e81ac"/
    s/static char hidnormfgcolor\[\]             = "#005577"/static char hidnormfgcolor\[\]             = "#5e81ac"/
    s/static char hidselfgcolor\[\]              = "#227799"/static char hidselfgcolor\[\]              = "#81a1c1"/
    s/static char hidnormbgcolor\[\]             = "#222222"/static char hidnormbgcolor\[\]             = "#2e3440"/
    s/static char hidselbgcolor\[\]              = "#222222"/static char hidselbgcolor\[\]              = "#2e3440"/
    s/static char urgfgcolor\[\]                 = "#bbbbbb"/static char urgfgcolor\[\]                 = "#8fbcbb"/
    s/static char urgbgcolor\[\]                 = "#222222"/static char urgbgcolor\[\]                 = "#88c0d0"/
    s/static char urgbordercolor\[\]             = "#ff0000"/static char urgbordercolor\[\]             = "#88c0d0"/
    s/static char urgfloatcolor\[\]              = "#db8fd9"/static char urgfloatcolor\[\]              = "#88c0d0"/
    s/static char scratchselfgcolor\[\]          = "#FFF7D4"/static char scratchselfgcolor\[\]          = "#d8dee9"/
    s/static char scratchselbgcolor\[\]          = "#77547E"/static char scratchselbgcolor\[\]          = "#2e3440"/
    s/static char scratchselbordercolor\[\]      = "#894B9F"/static char scratchselbordercolor\[\]      = "#A3BE8C"/
    s/static char scratchselfloatcolor\[\]       = "#894B9F"/static char scratchselfloatcolor\[\]       = "#A3BE8C"/
    s/static char scratchnormfgcolor\[\]         = "#FFF7D4"/static char scratchnormfgcolor\[\]         = "#d8dee9"/
    s/static char scratchnormbgcolor\[\]         = "#664C67"/static char scratchnormbgcolor\[\]         = "#2e3440"/
    s/static char scratchnormbordercolor\[\]     = "#77547E"/static char scratchnormbordercolor\[\]     = "#8FBCBB"/
    s/static char scratchnormfloatcolor\[\]      = "#77547E"/static char scratchnormfloatcolor\[\]      = "#8FBCBB"/
}' "$confdir/config.h"

## config.h continued
# Note the use of double quotes for attempts to match 's' resulted with no match or errors (\'s\').
sed -i "s/RULE(.instance = \"spterm\", .scratchkey = 's', .isfloating = 1)/RULE(.instance = \"spterm\", .scratchkey = 's', .isfloating = 1, .iscentered = 1)/" "$confdir/config.h"

## statuscmd patch
sed -i '/{ ClkStatusText,        0,                   Button3,        sigstatusbar,   {\.i = 3 } },/a \\t{ ClkStatusText,        ShiftMask,           Button1,        sigstatusbar,   {\.i = 4 } },\n	{ ClkStatusText,        ShiftMask,           Button2,        sigstatusbar,   {\.i = 5 } },\n	{ ClkStatusText,        ShiftMask,           Button3,        sigstatusbar,   {\.i = 6 } },' "$confdir/config.h"

## dwmblocks and conky
sed -i '/static const char \*const autostart\[\] = {/a \\t"dwmblocks", NULL,\n\t"conky", "-c", "/etc/conky/conky-dwm.conf", NULL,' "$confdir/config.h"
sed -i '/"st", NULL,/d' "$confdir/config.h"

## keybindings
sed -i '/static const char \*termcmd\[\]  = { "kitty", NULL };/a static const char \*texteditor\[\]  = { "kitty", "nvim", NULL };' "$confdir/config.h"
sed -i '/static const char \*termcmd\[\]  = { "kitty", NULL };/a static const char \*filemanager\[\]  = { "kitty", "vifm", NULL };' "$confdir/config.h"
sed -i '/static const char \*termcmd\[\]  = { "kitty", NULL };/a static const char \*rootfilemanager\[\]  = { "kitty", "sudo", "vifm", NULL };' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{ MODKEY|ShiftMask,             XK_o,          spawn,                  {.v = texteditor } },' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{ MODKEY|ControlMask,           XK_o,          spawn,                  {.v = filemanager } },' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{ MODKEY|ShiftMask|ControlMask, XK_o,          spawn,                  {.v = rootfilemanager } },' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{ MODKEY|ShiftMask|ControlMask, XK_p,          spawn,                  SHCMD("rofi-cheatmenu.sh") },' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{ MODKEY,                       XK_p,          spawn,                  SHCMD("rofi-favmenu.sh") },' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{ MODKEY|ShiftMask,             XK_p,          spawn,                  SHCMD("rofi-powermenu.sh") },' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{ MODKEY|ControlMask,           XK_p,          spawn,                  SHCMD("toolbox.sh") },' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{ MODKEY,                       XK_r,          spawn,                  SHCMD("rofi -modi drun#window#run#ssh -show drun -dpi 96") },' "$confdir/config.h" 
sed -i '/static const Key keys/a \\t{ MODKEY,                       XK_w,          spawn,                  SHCMD("wintitleinfo.sh") },' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{0,         XF86XK_TouchpadToggle,             spawn,                  SHCMD("touchpad-toggle.sh")},' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{0,         XF86XK_MonBrightnessDown,          spawn,                  SHCMD("backlight-brightness -5 ; kill -42 $(pidof dwmblocks)")},' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{0,         XF86XK_MonBrightnessUp,            spawn,                  SHCMD("backlight-brightness +5 ; kill -42 $(pidof dwmblocks)")},' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{0,         XF86XK_AudioMute,                  spawn,                  SHCMD("amixer -M -q set Master toggle ; kill -41 $(pidof dwmblocks)")},' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{0,         XF86XK_AudioLowerVolume,           spawn,                  SHCMD("amixer -M -q set Master 5%- ; kill -41 $(pidof dwmblocks)")},' "$confdir/config.h"
sed -i '/static const Key keys/a \\t{0,         XF86XK_AudioRaiseVolume,           spawn,                  SHCMD("amixer -M -q set Master 5%+ ; kill -41 $(pidof dwmblocks)")},' "$confdir/config.h"

## rules
# xprop(1):
#	 *	WM_CLASS(STRING) = instance, class
#	 *	WM_NAME(STRING) = title
#
sed -i '{
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "kitty", .isterminal = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "antiX Advert", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "antiX Root Persist", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "antiX - autoremove", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "antiX Calendar", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "antiX - Updater", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "antiX Volume", .isfloating = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "antix-user", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "antiXscreenshot", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Arandr", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "backlight", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "bootrepair", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "ceni", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "CENI set", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Clipit", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "Connman set", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "Connman System Tray", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Dconf-editor", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "Edit /etc/network/interfaces", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Font-viewer", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "galternatives", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Gksu", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Gksudo", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Ds-mouse.py", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "FastFlix", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "fastflix", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Galculator", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "GParted", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "Grsync", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Gtkhash", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Gtkdialog", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "Information", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "iso-snapshot", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "qt5ct", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "KeePassXC", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "KvantumViewer", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Kvantum Manager", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "live-usb-maker-gui-antix", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Lxappearance", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Lxtask", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "MakeMKV BETA", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "Manage Date and Time Settings", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "minstall", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "New VM", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "network-assistant", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "Select Default ALSA card", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Snapper-gui", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "Switch Wi-Fi program", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Synaptic", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "System-config-printer.py", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "system-keyboard-qt", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "timezone", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "toolbox", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "Virt-manager", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.class = "wpa_gui", .isfloating = 1, .iscentered = 1)
    /RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)/a \\tRULE(.title = "Color", .isfloating = 1, .iscentered = 1)
}' "$confdir/config.h"

## XF86 media keys
sed -i '/^\/\* See LICENSE file for copyright and license details/a \\n\/\* XF86 Media Keys \*\/\
#include <X11/XF86keysym\.h>' "$confdir/config.h"

## Delete default tag assignments
sed -i '/RULE(.class = "Gimp", .tags = 1 << 4)/d' "$confdir/config.h"
sed -i '/RULE(.class = "Firefox", .tags = 1 << 7)/d' "$confdir/config.h"

## Delete conflicting and redundant keys
sed -i '/MODKEY|Mod4Mask,              XK_u,          incrgaps/d' "$confdir/config.h"
sed -i '/MODKEY|Mod4Mask,              XK_i,          incrigaps/d' "$confdir/config.h"
sed -i '/MODKEY|Mod4Mask,              XK_o,          incrogaps/d' "$confdir/config.h"

# Compile and install binaries
make -C "$confdir" clean install

printf "\nSuccess!\n\n"

exit
