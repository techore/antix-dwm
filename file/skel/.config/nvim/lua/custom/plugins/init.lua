-- example file i.e lua/custom/init.lua
return {

    ["vifm/vifm.vim"] = {},

    ["vimwiki/vimwiki"] = {
      config = function()
         require "custom.plugins.vimwiki"
      end,
   },
}
-- load your globals, autocmds here or anything .__.
