vim.g.vimwiki_list = {
    {path = '~/wikis/vimwiki/', syntax = 'markdown', ext = '.md'},
    {path = '~/wikis/vimwiki2/', syntax = 'markdown', ext = '.md'},
    {path = '~/wikis/vimwiki3/', syntax = 'markdown', ext = '.md'}
}
vim.g.vimwiki_ext2syntax = {['.md'] = 'markdown', ['.markdown'] = 'markdown', ['.mdown'] = 'markdown'}
