#!/bin/env bash
# Project: antix-dwm
# Location: /etc/profile.d/axdwm-envs.sh
# Dependency:
# Description: Set bash environment variables which are also used by fish via fenv.
# Usage: Copy axdwm-envs.sh to /etc/profile.d/.

if test $UID -ge 1000 ; then
    export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games"
    if test -d $HOME/.local/bin ; then
        export PATH="$HOME/.local/bin:$PATH"
    fi
fi

export TERMINAL="kitty"
export EDITOR="nvim"
export VISUAL="nvim"
export GDK_SCALE="1"
export GDK_DPI_SCALE="1.0"
export QT_AUTO_SCREEN_SCALE_FACTOR="1"
