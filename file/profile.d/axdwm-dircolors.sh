#!/bin/env bash
# Project: antix-dwm
# Location: /etc/profile.d/axdwm-dircolors.sh
# Dependency:
# Description: Setup for /bin/ls and /bin/grep to support color. The alias is in /etc/bash.bashrc. 
# Usage: Copy axdwm-dircolors.sh to /etc/profile.d/.

if test -f "/etc/dircolors" ; then
        eval $(dircolors -b /etc/dircolors)
fi

if test -f "$HOME/.dircolors" ; then
        eval $(dircolors -b $HOME/.dircolors)
fi
