# Project: antix-dwm
# Location: /etc/fish/conf.d

if status is-interactive
  fenv source /etc/profile.d/axdwm-envs.sh
end

# Enable fish vi mode. Comment or delete to disable vi mode.
fish_vi_key_bindings
