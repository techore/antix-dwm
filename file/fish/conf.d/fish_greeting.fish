# Customize interactive shell greeting
# Also, provide a count of task +in.
function fish_greeting
    if test -f /usr/bin/task; or test -f /usr/local/bin/task
        set INCOUNT (task rc.verbose=nothing +in +PENDING count)
        printf "\nWelcome to antiX dwm! Powered by Debian.\nTaskwarrior inbox: $INCOUNT\n\n"
    else
        printf "\nWelcome to antiX dwm! Powered by Debian.\n\n"
    end
end
