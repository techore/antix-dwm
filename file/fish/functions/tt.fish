function tt --wraps=taskwarrior-tui --description 'alias tt taskwarrior-tui'
    if test -f /usr/bin/task; or test -f /usr/local/bin/task
    	if test -f /usr/bin/taskwarrior-tui; or test -f /usr/local/bin/taskwarrior-tui
            taskwarrior-tui $argv;
	end
    else
        printf "\nTaskwarrior or taskwarrior-tui is not installed!\n\n"; 
    end
end
