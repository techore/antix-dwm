# Defined in - @ line 1
function s --wraps='kitty +kitten ssh' --description 'alias s=kitty +kitten ssh'
    if test -f /usr/bin/ssh
        kitty +kitten ssh $argv;
    else
        printf "\nssh is not installed!\n\n"; 
    end
end
