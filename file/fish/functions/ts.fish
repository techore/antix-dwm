function ts --description 'alias ts = task sync'
    if test -f /usr/bin/task; or test -f /usr/local/bin/task
        task sync $argv;
    else
        printf "\nTaskwarrior is not installed!\n\n"; 
    end
end
