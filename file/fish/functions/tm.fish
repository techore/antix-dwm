function t --description 'alias t = task modify'
    if test -f /usr/bin/task; or test -f /usr/local/bin/task
        task modify $argv; 
    else
        printf "\nTaskwarrior is not installed!\n\n"; 
    end
end
