function nr --description 'alias nr = nvim -R'
    if test -f /usr/bin/nvim
        nvim -R $argv;
    else
        printf "\nneovim is not installed!\n\n"; 
    end
end
