function nv --wraps=nvim --description 'alias nv nvim'
    if test -f /usr/bin/nvim
        nvim  $argv;
    else
        printf "\nneovim is not installed!\n\n"; 
    end
end
