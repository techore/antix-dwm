function in --description 'alias in = task add +in'
    if test -f /usr/bin/task; or test -f /usr/local/bin/task
        task add +in $argv; 
    else
        printf "\nTaskwarrior is not installed!\n\n"; 
    end
end

