function les --description 'alias les less -R'
end
function in --description 'alias in = task add +in'
    if test -f /usr/bin/less
        less -R $argv;
    else
        printf "\nless is not installed!\n\n"; 
    end
end
