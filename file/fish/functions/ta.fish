function t --wraps=task --description 'alias t = task add'
    if test -f /usr/bin/task; or test -f /usr/local/bin/task
        task add $argv; 
    else
        printf "\nTaskwarrior is not installed!\n\n"; 
    end
end
