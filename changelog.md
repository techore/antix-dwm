# Change Log

## next release

antiX 22 runit dwm Unofficial, release 3

**Download:**

**Changes:**

1. rofi-favmenu.sh: update default menu items to text editor, file manager, internet browser, youtube fzf, and keepassxc
2. add key mod+w to display window title and class via dunst; wintitleinfo.sh
3. pkgs-systools.sh: add gpg, hashrat, dnsutils
4. update inst-fastflix.sh: FastFlix 5.5.6, NVenc 7.28, & hdr10plus_tool 1.6.0
5. add ratpass bash script to generate password
6. update shell scripts to use debian/devuan compatible "env" references
7. update inst-virtmanager.sh: added runit service files
8. add /etc/antix-dwm_release file
9. add prebuild-mpr package repository; makedeb, mist, and neovim
10. update neovim 0.9.1

## January 23, 2023 

antiX 22 runit dwm Unofficial, release 2

**Download:** https://archive.org/details/antix-22-runit-dwm-rel2_202301

**Changes:**

1. index.md: add ceni to "Install process"
2. fish: enable vi mode system-wide
3. sysctl: permit use of ping by users
4. st: add st as an alternative lightweight terminal
5. cheatsheet: add st
6. Toolbox menu: now uses st not kitty to reduce memory footprint
7. dwm: conf-dwm-rmkitty.sh script to configure dwm to use st versus kitty
8. connman: add connman and cmst
9. bluetooth: inst-bluez.sh for bluetooth support with connman
10. cheatsheet: fish
11. conky: add xinerama_head for multiple display support
12. fonts: update Nerd fonts to version 2.3.1
13. file manager: vifm is replacing lf
14. dwm: keybindings
    - delete mod+ctrl+l for lf
    - add mod+ctrl+o for vifm
    - add mod+shift+o for nvim
15. connman: disable url checks
16. vifm.vim: integration between vifm and nvim
17. cheatsheet: add vifm
18. font: add Symbola for st glyph support
19. dwm: add XF86TouchpadToggle support
20. runit: configure logs for anacron, connman, cron, dbus, and haveged 
    - Logs are not written to /var/log/runit/ as intended, however, logs are written to /var/log/daemon.log
21. neovim: update from 8.0 to 8.2

## January 11, 2023

antiX 22 runit dwm Unofficial, initial release

**Download:** https://archive.org/details/antix-22-runit-dwm
