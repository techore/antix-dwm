#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/font-source_sans.sh
# Dependency: curl
# Description: download and copy Adobe Source Sans font.
# Usage: sudo font-source_sans.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

fonts_dir="/usr/local/share/fonts/SourceSans3"

printf "  Installing Source Sans font\n\n"

if test -d "$fonts_dir"; then
  rm -fr "$fonts_dir"
fi

mkdir "$fonts_dir"
cd "$fonts_dir" || exit

curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-Black.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-BlackIt.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-Bold.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-BoldIt.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-ExtraLight.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-ExtraLightIt.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-It.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-Light.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-LightIt.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-Regular.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-Semibold.otf
curl -LO https://github.com/adobe-fonts/source-sans/raw/release/OTF/SourceSans3-SemiboldIt.otf 
