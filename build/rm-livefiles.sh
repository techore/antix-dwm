#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/rm-livefiles.sh
# Dependency:
# Description: add or delete items to /usr/local/share/live-files/ to prevent including using iso-snapshot call remaster-antix.
# Usage: sudo ./rm-livefiles.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "  Deleting extraneous antiX live-files\n\n"
    # Cleanup default files supporting antiX window managers.
    rm -fr /usr/local/share/live-files/files/etc/desktop-session
    rm -fr /usr/local/share/live-files/files/etc/skel/.fluxbox
    rm -fr /usr/local/share/live-files/files/etc/skel/.icewm
    rm -fr /usr/local/share/live-files/files/etc/skel/.jwm
fi
