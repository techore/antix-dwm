#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/font-nerd_fonts.sh
# Dependencies: wget, unzip, rename
# Description: download and copy Nerd fonts.
# Usage: sudo font-nerds_fonts.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

declare -a fonts=(
    NerdFontsSymbolsOnly
)

version='2.3.1'
fonts_dir="/usr/local/share/fonts/NerdFonts"

printf "  Installing Nerd font(s)\n\n"

if test -d "$fonts_dir"; then
    rm -fr "$fonts_dir"
fi    

mkdir -p "$fonts_dir"

for font in "${fonts[@]}"; do
    zip_file="${font}.zip"
    download_url="https://github.com/ryanoasis/nerd-fonts/releases/download/v${version}/${zip_file}"
    printf %s "Downloading $download_url\n"
    curl -LO "$download_url"
    unzip "$zip_file" -d "$fonts_dir"
    rm "$zip_file"
done

find "$fonts_dir" -name '*Windows Compatible*' -delete
