#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/inst-vifm.sh
# Dependency:
# Description: Install lf file manager binary to /usr/local/bin.
# Usage: sudo inst-vifm.sh

if test $EUID -gt 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
  printf "  Installing vifm file manager"
  apt -y install vifm
  cp -rf /usr/local/src/antix-dwm/file/vifm/* /usr/share/vifm/
fi
