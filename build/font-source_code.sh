#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/font-source_code.sh
# Dependency: curl
# Description: download and copy Adobe Source Code font.
# Usage: sudo font-source_code.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

fonts_dir="/usr/local/share/fonts/SourceCodePro"

printf "  Installing Source Code font\n\n"

if test -d "$fonts_dir"; then
  rm -fr "$fonts_dir"
fi

mkdir "$fonts_dir"
cd "$fonts_dir" || exit

curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-Black.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-BlackIt.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-Bold.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-BoldIt.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-ExtraLight.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-ExtraLightIt.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-It.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-Light.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-LightIt.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-Medium.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-MediumIt.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-Regular.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-Semibold.otf
curl -LO https://github.com/adobe-fonts/source-code-pro/raw/release/OTF/SourceCodePro-SemiboldIt.otf 
