#!/usr/bin/env bash
# Project: antix-dwm
# Location: antixdwm/build/git-fenv.sh
# Dependencies:
# Description: borrowing fenv plugin from on-my-fish.
# Usage: sudo git-fenv.sh

if test $EUID -gt 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "  Installing plugin-foreign-env source\n\n"
    rm -fr /usr/local/src/plugin-foreign-env
    git clone https://github.com/oh-my-fish/plugin-foreign-env /usr/local/src/plugin-foreign-env
    cp /usr/local/src/plugin-foreign-env/functions/* /etc/fish/functions/
fi
