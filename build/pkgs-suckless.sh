#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/pkgs-suckless.sh
# Dependency:
# Description: install suckless build dependencies.
# Usage: sudo pkgs-suckless.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "  Installing suckless build dependencies\n\n"
    apt --yes install \
        build-essential git libx11-dev libxft-dev libxinerama-dev libxrandr-dev
fi 
