#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/git-st.sh
# Dependencies: build-essentials libx11-dev libxft-dev git
# Description: suckless dynamic window manager (dwm) via 
#   https://github.com/bakkeby/st-flexipatch
# Usage: sudo git-st.sh

if test $EUID -gt 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1

else
    printf "  Downloading dwm-flexipatch source\n\n"
    rm -fr /usr/local/src/st-flexipatch
    git clone https://github.com/bakkeby/st-flexipatch /usr/local/src/st-flexipatch
    cd /usr/local/src/st-flexipatch
    make
fi
