#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/font-material_design.sh
# Dependency: curl
# Description: download and copy Google's Material Design Icons.
# Usage: sudo font-material_design.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

fonts_dir="/usr/local/share/fonts/MaterialIcons"

printf "  Installing Material Design font\n\n"

if test -d "$fonts_dir"; then
   rm -fr "$fonts_dir"
fi

mkdir "$fonts_dir"
cd "$fonts_dir" || exit

curl -LO https://github.com/google/material-design-icons/raw/master/font/MaterialIcons-Regular.ttf
curl -LO https://github.com/google/material-design-icons/raw/master/font/MaterialIconsOutlined-Regular.otf
curl -LO https://github.com/google/material-design-icons/raw/master/font/MaterialIconsRound-Regular.otf
curl -LO https://github.com/google/material-design-icons/raw/master/font/MaterialIconsSharp-Regular.otf
curl -LO https://github.com/google/material-design-icons/raw/master/font/MaterialIconsTwoTone-Regular.otf
