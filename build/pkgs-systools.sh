#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/pkgs-systools.sh
# Dependency:
# Description: install system tool packages.
#     lm-sensors for ixni sys temps and fan speeds
#     imagemagick for ytfzf using kitty as thumbnail viewer
#     makedeb and mist for prebuilt-mpr
# Usage: sudo pkgs-systools.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "  Installing system cli tool packages.\n\n"
    apt --yes install \
        acpi-support-base acpid antix-goodies arch-install-scripts \
        btop connman deborphan dnsutils \
        efibootmgr fish fontconfig \
        galculator gpg gpg-agent gpm \
        hashrat hunspell-en-us \
        imagemagick iotop-c \
        kitty laptop-detect lm-sensors \
        makedeb mediainfo mist \
        openssh-client p7zip-full \
        ripgrep tmux tree unzip vifm
fi 
