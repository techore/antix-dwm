#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/git-dircolors.sh
# Dependencies: git
# Description: nord dircolors for $LS_COLORS
# Usage: sudo git-dircolors.sh

if test $EUID -gt 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1

else
    printf "  Downloading nord-dircolors source\n\n"
    rm -f /etc/dircolors
    rm -fr /usr/local/src/nord-dircolors
    git clone https://github.com/arcticicestudio/nord-dircolors.git /usr/local/src/nord-dircolors
    ln -s /usr/local/src/nord-dircolors/src/dir_colors /etc/dircolors
fi
