#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/git-dwm.sh
# Dependencies: build-essentials libx11-dev libxft-dev libxinerama-dev git
# Description: suckless dynamic window manager (dwm) via
#   https://github.com/bakkeby/dwm-flexipatch
# Usage: sudo git-dwm.sh

if test $EUID -gt 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1

else
    printf "  Downloading dwm-flexipatch source\n\n"
    rm -fr /usr/local/src/dwm-flexipatch
    git clone https://github.com/bakkeby/dwm-flexipatch /usr/local/src/dwm-flexipatch
    cd /usr/local/src/dwm-flexipatch
    make
fi
