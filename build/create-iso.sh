#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/create-iso.sh
# Dependency:
# Description: Create antix-core dwm ISO to publish.
# Usage: sudo ./create-iso.sh

if test $EUID -gt 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit
else
    while true; do
        printf "\nProvide ISO file name.\n"
        printf "For example, antiX-22-runit-dwm-unofficial_x64.iso\n"
        read -p "ISO file name: " isoname
        printf "\n"
        read -p "Is $isoname correct (y/N)? " response
        if test $response = y || test $response = Y ; then
            #printf "\n=========>  Enabling snapshot editing\n"
            #sed -i 's/edit_boot_menu=no/edit_boot_menu=yes/' /etc/iso-snapshot.conf
            printf "=========>  Running tasks\n"
            iso-snapshot -c -r -x Desktop -x Documents -x Downloads -x Music -x Networks -x Pictures -x Steam -x Videos -x Virtualbox -f "$isoname"
            printf "\n=========>  ISO written to /home/snapshot/$isoname.\n"
            printf "=========>  Done!\n"
            exit 0
        else
            exit 
        fi
    done
fi
