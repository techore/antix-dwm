#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/git-dwmblocks.sh
# Depedencies: build-essential libx11-dev libxft-dev git
# Description: mouse support for executing scripts from statusbar. Fork of 
#   https://github.com/torrinfail/dwmblocks.git.
# Usage: sudo git-dwmblocks.sh

if test $EUID -gt 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "  Downloading dwmblocks and statuscmd patch sources\n\n"
    rm -fr /usr/local/src/dwmblocks
    git clone https://github.com/torrinfail/dwmblocks.git /usr/local/src/dwmblocks
    curl -LO --output-dir /usr/local/src/dwmblocks https://dwm.suckless.org/patches/statuscmd/dwmblocks-statuscmd-20210402-96cbb45.diff
    cd /usr/local/src/dwmblocks
    patch -p1 < dwmblocks-statuscmd-20210402-96cbb45.diff
    make
fi
