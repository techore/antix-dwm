#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/pkgs-audio.sh
# Dependency:
# Description: install alsa tool packages.
# Usage: sudo pkgs-audio.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "  Installing alsa tool packages.\n\n"
    apt --yes install \
        alsa-tools alsa-utils alsamixer-equalizer-antix libasound2-plugin-equal
fi 
