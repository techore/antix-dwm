#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/git-themes.sh
# Dependency: lxappearance, librsvg2-common, qt5ct, and qt5-style-kvantum packages
# Description: Install gtk2/3 and qt5 themes from github repositories.
# Usage: sudo git-themes.sh

if test $EUID -gt 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "  Installing Nord themes\n\n"
    
    # eliverlara's Nordic theme
    rm -fr /usr/local/src/Nordic
    git clone https://github.com/EliverLara/Nordic.git /usr/local/src/Nordic

    rm -fr /usr/share/themes/Nordic
    ln -s /usr/local/src/Nordic /usr/share/themes/

    rm -fr /usr/share/icons/Nordic-*
    ln -s /usr/local/src/Nordic/kde/cursors/Nordic-cursors /usr/share/icons/

    for d in /usr/local/src/Nordic/kde/folders/* ; do
	ln -s "$d" /usr/share/icons/
    done

    rm -fr /usr/share/Kvantum/Nordic*
    for d in /usr/local/src/Nordic/kde/kvantum/* ; do
	ln -s "$d" /usr/share/Kvantum/
    done

    rm -fr /usr/share/color-schemes/Nordic*
    rm -fr /usr/share/color-schemes/nordic*
    for f in /usr/local/src/Nordic/kde/colorschemes/* ; do
	ln -s "$f" /usr/share/color-schemes/
    done

    # alvatips's Nordzy cursors
    rm -fr /usr/local/src/Nordzy-cursors
    git clone https://github.com/alvatip/Nordzy-cursors.git /usr/local/src/Nordzy-cursors
    rm -fr /usr/share/icons/Nordzy-*
    ln -s /usr/local/src/Nordzy-cursors/Nordzy-cursors /usr/share/icons/
    ln -s /usr/local/src/Nordzy-cursors/Nordzy-cursors-lefthand /usr/share/icons/
    ln -s /usr/local/src/Nordzy-cursors/Nordzy-cursors-white /usr/share/icons/

    # alvatip's Nordzy icons
    rm -fr /usr/local/src/Nordzy-icon
    git clone https://github.com/alvatip/Nordzy-icon.git /usr/local/src/Nordzy-icon
    cd /usr/local/src/Nordzy-icon/ && ./install.sh --theme all 

    # zayronixio's Zafiro Nord Black (Dark) icons
    #rm -fr /usr/local/src/Zafiro-Nord-Dark
    #git clone https://github.com/zayronxio/Zafiro-Nord-Dark /usr/local/src/Zafiro-Nord-Dark
    #rm -fr /usr/share/icons/Zafiro-Nord-Black
    #cp -r /usr/local/src/Zafiro-Nord-Dark /usr/share/icons/Zafiro-Nord-Black
    #sed -i 's/IconTheme=Zafiro-icons/IconTheme=Zafiro-Nord-Black/' /usr/share/themes/Nordic/index.theme

fi
