#!/usr/bin/env bash
# Location: /usr/local/src/antix-dwm/inst/rm-stuff.sh
# Dependency:
# Description: remove misc. packages.
# Usage: sudo ./rm-stuff.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "  Removing packages\n\n"
    apt --yes remove mc mc-data ppp gnome-ppp
fi 
