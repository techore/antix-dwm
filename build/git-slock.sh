#!/usr/bin/env bash
# Project: antix-dwm
# Location: antixdwm/build/git-slock.sh
# Dependencies: build-essentials (this needs work) 
# Description: suckless screen lock (slock). Fork of
#   https://github.com/bakkeby/slock-flexipatch.
# Usage: sudo git-slock.sh

if test $EUID -gt 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "  Downloading slock-flexipatch source\n\n"
    rm -fr /usr/local/src/slock-flexipatch
    git clone https://github.com/bakkeby/slock-flexipatch /usr/local/src/slock-flexipatch
    cd /usr/local/src/slock-flexipatch
    make
fi
