#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/pkgs-ui.sh
# Dependency:
# Description: install suckless build dependencies.
# Usage: sudo pkgs-ui.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "  Installing ui packages\n\n"
    apt --yes install \
        aptitude cmatrix conky curl dunst jq libnotify-bin \
        libyajl-dev rofi util-linux xss-lock
fi 
