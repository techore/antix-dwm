#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/pkgs-xorg.sh
# Dependency:
# Description: install xorg packages.
# Usage: sudo pkgs-xorg.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "  Installing xorg packages.\n\n"
    apt --yes install xorg xinput xserver-xorg-legacy ddm-mx
fi 
