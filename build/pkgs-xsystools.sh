#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/pkgs-xsystools.sh
# Dependency:
# Description: install xorg system tool packages.
# Usage: sudo pkgs-xsystools.sh
# Notes:
#   swallow patch deps: libx11-xcb-dev and libxcb-res0-dev
#   iso-snapshot needs leafpad to menu pause
#   wmctrl is needed for ratpass

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "  Installing xorg system tool packages.\n\n"
    apt --yes install \
        advert-block-antix antix-installer antix-libs antix-user arandr \
        at-spi2-core \
        bootrepair-antix \
        clipit cmst \
        dconf-editor droopy-antix \
        fonts-symbola \
        galternatives gksu gparted grsync gtk2-engines \
        gtk2-engines-murrine gtk2-engines-pixbuf gtkhash \
        inxi-gui-antix iso-snapshot-antix \
        keepassxc \
        leafpad librsvg2-common libseat1 libx11-xcb-dev libxcb-res0-dev \
        links2 live-usb-maker-gui-antix lxappearance lxtask \
        mesa-utils \
        network-assistant nvidia-detect \
        packageinstaller partimage python3-xdg \
        qt5-style-kvantum qt5-style-kvantum-themes qt5ct \
        repo-manager runit-service-manager \
        screenshot-antix seatd synaptic system-config-printer \
        system-config-printer-udev \
        wallpaper-backgrounds-antix17 wallpaper-plain-antix17 wmctrl \
        xdg-user-dirs xdg-utils xdotool xsel
fi
