#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/install/inst-freeplane.sh
# Dependency:
# Description: Install lf file manager binary to /usr/local/bin.
# Usage: sudo inst-freeplane.sh

if test $EUID -gt 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n=========>  Installing Freeplane java runtime environment (dependency)\n\n"
    apt -y install default-jre
    printf "=========>  Downloading Freeplane mind mapping software\n\n"
    rm -fr /tmp/freeplane
    mkdir /tmp/freeplane
    curl -L https://sourceforge.net/projects/freeplane/files/freeplane%20stable/freeplane_1.10.6u1~upstream-1_all.deb --output /tmp/freeplane/freeplane.deb
    printf "=========>  Installing Freeplane mind mapping software\n\n"
    dpkg -i /tmp/freeplane/freeplane.deb
    printf "=========>  Freeplane installation complete!\n\n"
    printf "=========>  Import antix-dwm Freeplane configuration by\n"
    printf "=========>  launching Freeplane then selecting:\n"
    printf "=========>  Tools menu --> Preferences menu --> Load button\n"
    printf "=========>  and selecting the configuration file found at\n"
    printf "=========>  \usr\local\src\antix-dwm\install\antix-dwm.freeplaneoptions\n\n"
fi
