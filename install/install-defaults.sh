#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/src/antix-dwm/install/install-defaults.sh
# Dependency:
# Description: wrapper to execute my common inst-[script]s.
# Usage: sudo install-defaults.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    source /usr/local/src/antix-dwm/install/inst-avapps.sh
    source /usr/local/src/antix-dwm/install/inst-miscapps.sh
    source /usr/local/src/antix-dwm/install/inst-prodapps.sh
fi 
