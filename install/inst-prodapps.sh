#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/src/antix-dwm/install/inst-prodapps.sh
# Dependency:
# Description: install system tool packages.
# Usage: sudo inst-prodapps.sh
# Notes:
#   If abiword is insufficient for Microsoft Word document editing,
#   libreoffice-writer is excellent alternative but comes with 50+ 
#   packages and ~300 MB disk space.

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n=========>  Installing productivity packages.\n\n"
    apt --yes install atril gnumeric qutebrowser
    printf "\n=========>  Do you need a MS Word compatible editor?\n\n"
    printf "=========>  Package abiword is lightweight and can edit basic\n"
    printf "=========>  Word documents (richtext). Otherwise, install \n"
    printf "=========>  package libreoffice-writer at ~50 packages and\n"
    printf "=========>  ~300 MB disk space."
fi 
