#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/src/antix-dwm/install/inst-touchpadtoggle-f9.sh
# Dependency:
# Description: Sets keycode for f9 to XF86TouchpadToggle in user .xinitrc.
# Usage: sudo inst-touchpadtoggle-f9.sh

if test $EUID -gt 0; then
    printf "\n=========>  For use with Asus ROG G750JX\n"
    printf "=========>  Assigning keycode (f9) to XF86TouchpadToggle\n\n"
    sed -i '/exec dwm/i xmodmap -e "keycode 75 = XF86TouchpadToggle"' ~/.xinitrc
    cat ~/.xinitrc
    printf "\n=========>  Assignment complete!\n\n"
else
    printf "\n    Must not be root or using sudo!\n\n"
    exit 1
fi
