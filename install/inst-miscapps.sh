#!/usr/bin/env bash
# Location: /usr/local/src/antix-dwm/install/inst-miscapps.sh
# Dependency:
# Description: install misc. packages.
# Usage: sudo inst-miscapps.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n=========>  Installing misc. application packages.\n\n"
    apt --yes install gnome-mahjongg
fi 
