#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/src/antix-dwm/install/inst-virtmanager.sh
# Dependency:
# Description: install virt-manager.
# Usage: sudo inst-virtmanager.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n=========>  Install virt-manager and utilities.\n\n"
    apt --yes install \
        bridge-utils dnsmasq gir1.2-spiceclientgtk-3.0 libvirt-clients \
        libvirt-daemon-system python3-dbus qemu qemu-system-x86 \
        qemu-utils virt-manager zstd

    printf "\n=========>  Disable dnsmasq runit service.\n\n"
    # dnsmasq is used for libvirt virtual machine leases, however, it
    # is not needed with connman and breaks name resolution on the host.
    touch /etc/service/dnsmasq/down

    printf "\n=========>  Cleanup!\n\n"
    # clean up of prior installation
    rm -fr /tmp/libvirt
    sv stop virtlogd
    sv stop virtlockd
    sv stop libvirtd
    rm -fr /etc/runit/runsvdir/default/libvirtd \
        /etc/runit/runsvdir/default/virtlockd \
        /etc/runit/runsvdir/default/virtlogd
    rm -fr /etc/service/libvirtd \
        /etc/service/virtlockd \
        /etc/service/virtlogd
    rm -fr /etc/sv/libvirtd \
        /etc/sv/virtlockd \
        /etc/sv/virtlogd
    
    printf "\n=========>  Install runit service units.\n\n"
    # git, copy, link, and start runit services
    git clone https://gitlab.com/techore/libvirt-runit.git /tmp/libvirt
    cp -r /tmp/libvirt/etc/* /etc/
    cp -r /tmp/libvirt/var/* /var/
    ln -s /etc/sv/libvirtd /etc/service/libvirtd
    ln -s /etc/sv/virtlockd /etc/service/virtlockd
    ln -s /etc/sv/virtlogd /etc/service/virtlogd
    printf "\n=========>  Start virtlogd, virtlockd, and libvirtd.\n\n"
    sv start virtlogd
    sv start virtlockd
    sv start libvirtd

    # Start and set to auto start virbr0, "default" virt-manager bridge.
    printf "\n=========>  Start and set to auto start the'default' NAT virtual bridge (virbr0).\n\n"
    virsh net-start default
    virsh net-autostart default
    
    # Execute inst-bridge.sh create a bridge.
    printf "\n=========>  Create bridge (no NAT) and bind physical interface.\n\n"
    source inst-bridge.sh

    printf "\n========>  Done!\n"
    printf "========>  Must add user accounts to libvirt group.\n"
    printf "========>  \$ sudo usermod -aG libvirt <username>\n\n"
fi
