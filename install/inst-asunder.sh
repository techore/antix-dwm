#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/src/antix-dwm/install/inst-asunder.sh
# Dependencies: flac wavpack lame for flac, wave, and mp3 support.
# Description: Asunder is a graphical Audio CD ripper and encoder.
# Usage: sudo inst-asunder.sh

if test $EUID -gt 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    # fastflix
    printf "\n=========>  Updating apt cache\n\n"
    apt update
    printf "\n=========>  Installing asunder\n\n"
    apt --yes install asunder flac wavpack lame
    printf "\n=========>  asunder install complete!\n\n"
fi
