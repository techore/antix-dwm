#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/src/antix-dwm/install/inst-remmina.sh
# Dependency:
# Description: install remmina and plugins for vnc and rdp.
# Usage: sudo ./inst-remmina.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "=========>  Updating apt cache\n\n"
    apt update
    printf "\n=========>  Installing remmina and plugin packages\n\n"
    apt --yes install remmina remmina-plugin-vnc remmina-plugin-rdp
    printf "\n=========>  remmina install complete!\n\n"
fi
