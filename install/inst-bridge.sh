#!/usr/bin/env bash
# Location: /usr/local/src/antix-dwm/install/inst-bridge.sh
# Dependency: bridge-utils
# Description: update /etc/network/interfaces to create bridge
#   sourced by inst-virtmanager.sh
# Usage: sudo inst-bridge.sh

if test $EUID -gt 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

# Bridge
# Using name "bridge" instead of "virbr" to distinguish from virsh created bridges.
read -p "Enter bridge name [bridge0]: " BRIDGE
BRIDGE=${BRIDGE:-bridge0}
printf "\n=========>  Assign $BRIDGE as bridge name.\n\n"
read -p "ENTER to continue or CTRL+c to exit."
printf "\n"

# Bridge port
read -p "Enter bridge port name [eth0]: " BPORT
BPORT=${BPORT:-eth0}
printf "\n=========>  Assign $BPORT as bridge port.\n\n"
read -p "ENTER to continue or CTRL+c to exit."
printf "\n"

# Install dependency
printf "\n========>  Install bridge-utils for bridge support.\n\n"
apt -y install bridge-utils

# Backup files
# Backup main.conf
printf "\n========>  Backup /etc/connman/main.conf.\n\n"
cp /etc/connman/main.conf "/etc/connman/main.conf.$(date '+%Y%m%d%H%M').bak"
ls -l /etc/connman/*.bak

# Backup interfaces
printf "\n========>  Backup /etc/network/interfaces.\n\n"
cp /etc/network/interfaces "/etc/network/interfaces.$(date '+%Y%m%d%H%M').bak"
ls -l /etc/network/*.bak

# Update /etc/connman/main.conf to blacklist bridge and bridge port
printf "\n  Add $BRIDGE and $BPORT to connman blacklist\n\n"
# Blacklist bridge
sed -i 's/^# NetworkInterfaceBlacklist/NetworkInterfaceBlacklist/' /etc/connman/main.conf
if ! grep -q "^NetworkInterfaceBlacklist.*$BRIDGE" /etc/connman/main.conf; then
	sed -i "s/^NetworkInterfaceBlacklist = /NetworkInterfaceBlacklist = $BRIDGE,/" /etc/connman/main.conf
fi
# Blacklist bridge port and vnet
if ! grep -q "^NetworkInterfaceBlacklist.*$BPORT" /etc/connman/main.conf; then
	sed -i "s/^NetworkInterfaceBlacklist = /NetworkInterfaceBlacklist = $BPORT,vnet,/" /etc/connman/main.conf
fi
grep NetworkInterfaceBlacklist /etc/connman/main.conf

# Update /etc/network/interface
printf "\n  Configure $BRIDGE and $BPORT in /etc/network/interfaces\n\n"
# Bridge port
if ! grep -q "$BPORT" /etc/network/interfaces; then
	printf "\nauto $BPORT\niface $BPORT inet manual\n" >> /etc/network/interfaces
else
	printf "\n=========>  $BPORT found in /etc/network/interfaces!"
	printf "\n=========>  Manual update for $BPORT may be needed.\n\n" 
fi
# Bridge
if ! grep -q "$BRIDGE" /etc/network/interfaces; then
	printf "\nauto $BRIDGE\niface $BRIDGE inet dhcp\n    bridge_ports $BPORT\n    bridge_stp off\n    bridge_fd 0\n    bridge_maxwait 0\n " >> /etc/network/interfaces
else
	printf "\n=========>  $BRIDGE found in /etc/network/interfaces!"
	printf "\n=========>  Manual update for $BRIDGE may be needed.\n\n" 
fi
cat /etc/network/interfaces

printf "\n\n=========>  Done!\n\n"
