#!/usr/bin/env bash
# Location: /usr/local/src/antix-dwm/install/neovim.sh
# Dependency:
# Description: Install neovim from github repository.
# Usage: sudo neovim.sh
# Notes:
#    Use `update-alternatives --get-selections` to list.
#    Use `update-alternatives --query editor` to see candidates path and priority.
#    Use `update-alternatives --install /usr/bin/editor editor /usr/bin/nvim 30` to install.
#    Use `update-alternatives --set editor /usr/bin/nvim` to set and overide auto-selection
#    based on priority.
#    Priority is selected based on hightest value.

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "  Installing neovim from github releases"
    apt --yes remove --purge neovim
    apt --yes autoremove
    cd /tmp
    curl -LO https://github.com/neovim/neovim/releases/download/v0.8.3/nvim-linux64.deb
    dpkg -i nvim-linux64.deb

    # Configure updates-alternatives with priority 30 to duplicate Debian package.
    update-alternatives --install /usr/bin/editor editor /usr/bin/nvim 30
    update-alternatives --install /usr/bin/ex ex /usr/libexec/neovim/ex 30
    update-alternatives --install /usr/bin/pico pico /usr/bin/nvim 30
    update-alternatives --install /usr/bin/rview rview /usr/libexec/neovim/rview 30
    update-alternatives --install /usr/bin/rvim rvim /usr/libexec/neovim/rvim 30
    update-alternatives --install /usr/bin/vi vi /usr/bin/nvim 30
    update-alternatives --install /usr/bin/view view /usr/libexec/neovim/view 30
    update-alternatives --install /usr/bin/vim vim /usr/bin/nvim 30
    update-alternatives --install /usr/bin/vimdiff vimdiff /usr/libexec/neovim/vimdiff 30
fi
