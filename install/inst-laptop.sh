#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/install/inst-laptop.sh
# Dependency: laptop-detect package
# Description: Update dwmblocks/blocks.h enable brightness and battery on statusbar.
# Usage: sudo ./inst-laptop.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
fi

if ! laptop-detect ; then
    printf "=========>  Not detected as a laptop!\n"
else
    printf "=========>  Laptop!\n"
    if test -f /usr/local/src/dwmblocks/blocks.h ; then
        sed -i 's/\/\/	{"",		"sb-battery\.sh/  	{"",		"sb-battery\.sh/' /usr/local/src/dwmblocks/blocks.h
        sed -i 's/\/\/	{"",		"sb-brightness\.sh/  	{"",		"sb-brightness\.sh/' /usr/local/src/dwmblocks/blocks.h
        cd /usr/local/src/dwmblocks
        make clean install
    else
        printf "========> missing blocks.h!"
        exit 1
    fi
fi

apt update
apt --yes install acpi-support
