#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/src/antix-dwm/install/inst-printer.sh
# Dependency:
# Description: install print service, cups, dependencies, and drivers.
# Usage: sudo inst-printer.sh

# Epson Printers: https://download.ebz.epson.net/dsc/search/01/search/

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "=========>  Updating apt cache\n\n"
    apt update
    printf "\n=========>  Installing cups print service and driver packages.\n\n"
    apt --yes install cups cups-bsd printer-driver-all 
    printf "\n=========>  Print service install complete!\n\n"
fi
