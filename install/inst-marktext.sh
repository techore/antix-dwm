#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/src/antix-dwm/install/inst-marktext.sh
# Dependency: 
# Description: https://github.com/marktext/marktext 
# package: https://github.com/marktext/marktext/releases/latest/marktext-amd64.deb
# Usage: sudo inst-marktext.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n=========>  Downloading and installing MarkText\n\n"
    rm -fr /tmp/fast/flix
    mkdir /tmp/fastflix
    cd /tmp/fastflix
    curl -LO https://github.com/marktext/marktext/releases/latest/download/marktext-amd64.deb
    printf "\n=========>  Removing existing MarkText\n\n"
    apt --yes remove marktext
    printf "\n=========>  Updating apt cache\n\n"
    apt update
    printf "\n=========>  Installing MarkText\n\n"
    dpkg -i marktext-amd64.deb 
    printf "\n=========>  MarkText install complete!\n\n"
fi
