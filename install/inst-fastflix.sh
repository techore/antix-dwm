#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/src/antix-dwm/install/inst-fastflix.sh
# Dependency: curl and unzip
# Description: Install fastflix binary and ffmpeg binary to ~/.local/bin/fastflix/
# and nvencc deb package.
# Usage: inst-fastflix.sh

if test $EUID -gt 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    # fastflix
    printf "\n=========>  Deleting FastFlix binary if any.\n\n"
    rm -f /usr/local/bin/fastflix
    rm -fr /tmp/ff
    mkdir /tmp/ff
    cd /tmp/ff
    printf "\n=========>  Downloading FastFlix archive.\n\n"
    curl -LO https://github.com/cdgriffith/FastFlix/releases/download/5.5.7/FastFlix_5.5.7_ubuntu-20.04_x86_64.zip
    printf "\n=========>  Copying FastFlix to /usr/local/bin/FastFlix.\n\n"
    unzip FastFlix_*
    chmod +x FastFlix
    cp FastFlix /usr/local/bin/FastFlix
    # The following were obtained using steps from AUR PKGBUILD.. good stuff! 
    # `icotool --extract --index=1 -o FastFlix.png fastflix/data/icon.ico`
    # icotool is from icoutils package from Debian repo.
    cp /usr/local/src/antix-dwm/install/FastFlix.desktop /usr/share/applications/
    cp /usr/local/src/antix-dwm/install/FastFlix.png /usr/share/icons/hicolor/256x256/apps/

    # nvencc
    printf "\n=========>  Removing existing nvencc package if any.\n\n"
    sudo apt --yes remove nvencc
    printf "\n=========>  Downloading nvencc deb package.\n\n"  
    curl -LO https://github.com/rigaya/NVEnc/releases/download/7.31/nvencc_7.31_Ubuntu20.04_amd64.deb
    printf "\n=========>  Install nvencc package and dependencies.\n\n"
    sudo apt update
    sudo apt --yes install libavformat58 libavfilter7 libass9
    sudo dpkg -i nvencc*

    # ffmpeg
    printf "\n=========>  Deleting ffmpeg binaries from /usr/local/bin/ if any.\n\n"
    rm -fr /usr/local/bin/ffmpeg
    rm -fr /usr/local/bin/ffmplay
    rm -fr /usr/local/bin/ffmprobe
    printf "\n=========>  Downloading ffmpeg archive.\n\n"
    curl -LO https://github.com/BtbN/FFmpeg-Builds/releases/download/latest/ffmpeg-master-latest-linux64-gpl.tar.xz
    printf "\n=========>  Copying binaries to /usr/local/bin/.\n\n"
    tar -xf ffmpeg-master-latest-linux64-gpl.tar.xz
    cp ffmpeg-master-latest-linux64-gpl/bin/ffmpeg /usr/local/bin
    cp ffmpeg-master-latest-linux64-gpl/bin/ffplay /usr/local/bin
    cp ffmpeg-master-latest-linux64-gpl/bin/ffprobe /usr/local/bin

    # hdr10plus_tool
    printf "\n=========>  Deleting hdr10plus_tool binary if any.\n\n"
    rm -fr /usr/local/bin/hdr10plus_tool
    rm -fr /tmp/hdr10
    mkdir /tmp/hdr10
    cd /tmp/hdr10
    printf "\n=========>  Downloading hdr10plus_tool archive.\n\n"
    curl -LO https://github.com/quietvoid/hdr10plus_tool/releases/download/1.6.0/hdr10plus_tool-1.6.0-x86_64-unknown-linux-musl.tar.gz
    printf "\n=========>  Copying hdr10plus_tool to /usr/local/bin/.\n\n"
    tar -xzvf hdr10plus_tool-1.6.0-x86_64-unknown-linux-musl.tar.gz
    cp hdr10plus_tool /usr/local/bin/

fi
