#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/src/antix-dwm/install/inst-bluez.sh
# Dependency:
# Description: install remmina and plugins for vnc and rdp.
# Usage: sudo ./inst-bluez.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "=========>  Updating apt cache\n\n"
    apt update
    printf "\n=========>  Installing bluez packages\n\n"
    apt --yes install bluez bluez-alsa-utils bluez-firmware bluez-obexd libasound2-plugin-bluez
    printf "\n=========>  bluez install complete!\n\n"
fi
