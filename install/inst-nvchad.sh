#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build/inst-nvchad.sh
# Dependency: neovim 9, ripgrep, npm packages
# Description: Install nvchad from github repository.
#   https://github.com/NvChad/NvChad
# Usage: sudo inst-nvchad.sh

if test $EUID -gt 0; then
    printf "\n    Must be root!\n\n"
    exit 1
else
    printf "\n  Installing nvchad to /etc/skel\n\n"
    if test ! -d "/etc/skel/.config/nvim"; then
        mkdir -p /etc/skel/.config/nvim
    fi
    rm -fr /usr/local/src/NvChad
    git clone https://github.com/NvChad/NvChad /usr/local/src/NvChad
    cp -r /usr/local/src/NvChad/* /etc/skel/.config/nvim/
    printf "\n  Installing package npm for node support\n\n"
    apt -y install npm
fi
