#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/src/antix-dwm/install/inst-avapps.sh
# Dependency:
# Description: install system tool packages.
# Usage: sudo inst-avapps.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n=========>  Installing audio/video application packages.\n\n"
    apt --yes install \
        celluloid cmus cmus-plugin-ffmpeg ffmpeg fzf gimp kodi \
        kodi-repository-kodi mirage mpv ueberzug yt-dlp ytfzf
fi 
