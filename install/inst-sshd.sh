#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/src/antix-dwm/install/inst-sshd.sh
# Dependency:
# Description: install ssh daemon and permit 22/tcp.
# Usage: sudo inst-sshd.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "=========>  Updating apt cache\n\n"
    apt update
    printf "\n=========>  Installing openssh-server package\n\n"
    apt --yes install openssh-server
    printf "\n=========>  Permit 22/tcp\n\n"
    ufw allow 22/tcp
    printf "\n=========>  sshd install complete!\n\n"
fi
