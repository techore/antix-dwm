#!/usr/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/install/inst-lf.sh
# Dependency:
# Description: Install lf file manager binary to /usr/local/bin.
# Usage: sudo inst-lf.sh

if test $EUID -gt 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
  printf "  Installing lf file manager"
  rm -fr /tmp/lf
  rm -f /usr/local/bin/lf
  mkdir /tmp/lf
  curl -L https://github.com/gokcehan/lf/releases/latest/download/lf-linux-amd64.tar.gz --output /tmp/lf/lf.tar.gz
  tar xzvf /tmp/lf/lf.tar.gz -C /usr/local/bin
  chmod +x /usr/local/bin/lf
fi
