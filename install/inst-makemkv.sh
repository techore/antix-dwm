#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/src/antix-dwm/install/inst-makemkv.sh
# Dependency: 
# Description: add gpg key, makemkv repository, and install makemkv.
# https://forum.makemkv.com/forum/viewtopic.php?f=3&t=24328
# package info: https://apt.benthetechguy.net
# Usage: sudo inst-makemkv.sh

if test $EUID -ne 0; then
    printf "\n    Must be root or using sudo!\n\n"
    exit 1
else
    printf "\n=========>  Downloading and installing calenhad.gpg\n\n"
    curl -L --url https://apt.benthetechguy.net/benthetechguy-archive-keyring.gpg --output /usr/share/keyrings/benthetechguy-archive-keyring.gpg
    printf "\n=========>  Creating apt source file makemkv.list\n\n"
    printf "deb [signed-by=/usr/share/keyrings/benthetechguy-archive-keyring.gpg] https://apt.benthetechguy.net/debian bullseye contrib non-free\n" > /etc/apt/sources.list.d/makemkv.list
    printf "=========>  Updating apt cache\n\n"
    apt update
    printf "\n=========>  Installing MakeMKV\n\n"
    apt --yes install makemkv
    printf "\n=========>  MakeMKV install complete!\n\n"
fi
