# Display driver

## Identify

Use `lspci` identify the graphic adapter and its driver. Alternatively, `lshw -businfo -class bridge -class display` may be used.

```
lspci | grep -e VGA -e 3D
```

## Intel

Results from an Intel CPU/GPU.

```
VGA compatible controller: Intel Corporation HD Graphics 630 (rev 04)
```

Use the reference below to determine the Xorg display driver results with `xserver-xorg-driver-intel`.

```
apt install xserver-xorg-video-intel
```

### nVidia 

Verify kernel headers are installed. Already installed in my experience.

```
sudo apt install linux-headers-$(uname -r)

```

Install nvidia proprietary driver for dedicated nVidia GPU or hybrid with Intel GPU. 

```
ddm-mx -i nvidia
```

Alternatively, uninstall teh proprietary driver and use the opensource driver

```
ddm-mx -p nvidia
```

Reboot after installation, login as user, and `startx` to verify operation before continuing.

**Notes**

An alternative to ddm-mx is to install using apt, `sudo apt install nvidia-driver`.


Command `nvidia-detect` will detect and display nvidia devices and recommend driver package.

Reference:
https://blog.thecurlybraces.com/2021/06/debian-bullseye-with-nvidia-hybrid-graphics
https://packages.debian.org/search?keywords=xserver-xorg-video

**Prime render offload notes**

On a hybrid laptop using Intel and nVidia, specify offload to the nVidia GPU for the command | command is the game binary.

```
__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia command
```

When using steam, right-mouse click the game, select properties, select "Set Launch Options" button to enter:

```
__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia %command%
```

Desktop shortcut or launcher an be updated to use nVidia GPU. Desktop shortcuts reside in `/usr/share/applications` or `~/.local/share/applications`.

```
find an example
```

Testing.

```
__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia glxinfo | grep "OpenGL renderer"
```
