# Frequently Asked Questions

1. How do I change the **`mod`** key to **`alt`** key for my keyboard doesn't have a **`win`** key?

    - Execute `cd /usr/local/src/dwm-flexipatch`
    - Execute `sudo vim config.h`
    - Type `/` and search for "define modkey" without the parenthesis and **`enter`**
    - Change Mod4Mask to Mod1Mask
    - Save changes and exit using `:wq`
    - Execute `sudo make clean install`

2. How do I get to a desktop or a graphical user interface? 
   
   After login, type `startx` and the enter key.
   
3. How do I change the font DPI?

    Edit ~/.Xresources and update the value for "Xft.dpi" to the preferred DPI.

4. How do I change the desktop wallpaper?

    By default the path /usr/share/wallpaper/axdwm/ is used to randomly select a background image at `startx` using `feh`. You can create a directory at ~/.config/wallpaper/, copy one or more wallpapers into the directory, then update the path in ~/.xinitrc, e.g. ~/.config/wallpaper. On next use of `startx`, it will display your wallpaper.

5. How do I change dwm to use my preferred terminal?

    antiX dwm uses the kitty terminal by default. This is done by using a script, /usr/local/bin/conf-dwm.sh, to configure /usr/local/src/dwm-flexipatch/config.h. To use a different terminal, replace the occurrences of kitty and its arguments with your preferred terminal and arguments, `sudo conf-dwm.sh /usr/local/src/dwm-flexipatch`, quit or reset dwm, then test. Don't forget to reapply conf-1440p.sh or conf-2160p.sh if needed.
    
    To use the suckless terminal (st) execute `sudo conf-dwm-rmkitty.sh /usr/local/src/dwm-flexipatch` then exit dwm. This will replace all kitty references with st.

6. How do I change from fish to my preferred shell?

    Execute `chsh` and provide the path to your preferred shell.

7. On mouse clicking the status bar, command X executes between 1 and 12 times.

    On testing antiX 4.9 kernels, statuscmd using SIGRTMIN resulted with very wonky behavior and executed script randomly between 1 and 12 times. On installing the 5.10 kernel, there were no further occurences. If you are experiencing this symptom using a newer kernel, experiment and please share your findings.

8. Where do I enabled a screen saver? How do I set the idle timeout? What about display power management (dpms)?
    
    Update ~/.xinitrc. There are several lines starting with xset and xss-lock. See the comments in .xinitrc and `xset` and `xss-lock` man pages for additional details.

9. I have enabled the screen saver as described above. How do I disable the screen lock?

    Update ~/.xinitrc by deleting `-- slock` but retain the `&` in the line starting with `feh`.
    
10. The backlight brightness after `startx` on my laptop is set for 50%. How do I disable or change the default value?

    Update ~/.xinitrc and the `backlight-brightness` line. This feature is commented out by default.

11. The sound volume after `startx` is set for 30%. How do I disable or change the default value?

    Update ~/.xinitrc and the `amixer` line. Comment or delete the line to disable the feature or update to your preferred value.
    
12. How do I disable conky or change the default configuration file?

    The conky process is started by dwm using the cool autostart patch. Update /usr/local/src/dwm-flexipatch/config.h by deleting or updating the conky line then `sudo make clean install`.
    
13. How do I move conky to use a different display?

    Update xinerama_head value in the conky configuration file. antiX dwm cheatsheets are found in /etc/conky/.

14. How do I force an application to float on launch?

    See `static const Rule rules[]` in the `/usr/local/src/dwm-flexipatch/config.h` file. Note the comments immediately after its declaration describing class and title. To identify a window's class or title, launch the program, open a terminal, execute `xprop`, then click the window. 
