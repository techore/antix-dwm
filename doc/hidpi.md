# HiDPI

The purpose of this article is to provide guidance on customizing antiX dwm Spin fonts, icons, and other UI elements.

## ~/.Xresources

```
Item        1080p     1440p     2160p (4k)
------------------------------------------
Xft.dpi:    96        128       192
```
## /etc/profile.d/axdwm-envs.sh

```
Item                            1080p     1440p     2160p (4k)
--------------------------------------------------------------
GDK_SCALE                       1                   2
GDK_DPI_SCALE                   1.0                 0.5
QT_AUTO_SCREEN_SCALE_FACTOR     1                   1
```

QT has additional variables that *may* be useful.
    - QT_AUTO_SCREEN_SET_FACTOR
    - QT_SCALE_FACTOR
    - QT_FONT_DPI

## /code/dwm-flexipatch

```
Item                                    1080p     1440p     2160p (4k)
--------------------------------------------------------------------
Material Icons Outlined:pixelsize       16        22        32
static const unsigned int borderpx      4         6         8
static const unsigned int gappih        10        14        20
static const unsigned int gappiv        10        14        20
static const unsigned int gappoh        10        14        20
static const unsighed int gappov        10        14        20
```

## /usr/share/rofi/themes/nord.rasi

```
Item                1080p     1440p     2160p (4k)
--------------------------------------------------
font                12        16          24
Window { border:    4         6           8
```

## /usr/share/rofi/themes/power.rasi and favorites.rasi

```
Item      1080p     1440p     2160p (4k)
----------------------------------------
Font      18        22        36
```

## /etc/conky/conky-[cheatsheet].conf

```
Item                1080p     1440p     2160p (4k)
--------------------------------------------------
gap_x               20        28          40
gap_y               30        44          60
minimum_width       400       520         800
```
Update `font` to change the default font and update `${font}` to update elements to your preference.

## Configuration scripts 

On determining the values you prefer, I would recommend copying antix-dwm/file/bin/conf-2160p.sh and conf-2160p-root.sh then updating them with preferred values. Rename the files and copy to /usr/local/bin to reapply the settings if needed after updating antix dwm.
