# Themes

The purpose of this article is to document theme details.

## Tools

The following binaries provide configuration for GTK, Qt5, and Kvantum.

1. GTK: lxappearance
2. Qt5: qt5ct
3. Kvantum: kvantummanager

## Packages

Theme packages. Dependencies are not listed unless a manual install is required.

1. lxappearance
2. qt5ct
3. qt5-style-kvantum
4. qt5-style-kvantum-themes

## System-wide files

1. themes: /usr/share/themes/
2. kvantum themes: /usr/share/Kvantum/
3. icons: /usr/share/icons/
4. color scheme: /usr/share/color-schemes/

## $HOME files

1. /.gtkrc-2.0
2. /.icons/
3. /.config/gtk-3.0/
4. /.config/Kvantum/
5. /.config/qt5ct/

## Icons

1. Icons include application icons, folder icons, and mouse cursors.
2. Each icon theme has an index.theme file defining the theme.
3. Only one icon theme can be selected, however, the "index.theme" for each icon set includes the "Inherits" attribute that specifies fall back icons themes for missing icons.  
