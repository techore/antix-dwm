# Known issues

## gksu

gksu and gksudo prompt does not permit saving password for the session or to keyring.

## mediakeys

Need to configure dwm to support additional media keys.

    - toggle wireless/airport
    - <f8> launches rofi favorites menu.. wild! I may just keep it.

## trackpad

Need to disable trackpad while typing.

## Nordic color theme
   - nemo: if using two panes, unselected pane's selected file foreground and background have no contrast and is unreadable.
   - antix qt tools: use a yellow background for some elements which looks hideous, e.g. iso-snapshot-antix

## virt-manager minimal install approach results with:
   - no network bridge interface

