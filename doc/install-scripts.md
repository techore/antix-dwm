# Installation scripts

## install-defaults.sh

install-defaults.sh executes:

1. inst-avapps.sh
2. inst-miscapps.sh
3. inst-prodapps.sh

## inst-avapps.sh

- celluloid: a mpv GUI
- cmus: music player
- gimp: graphics editor
- kodi: media center
- mirage: image viewer
- yt-dlp: youtube viewer

## inst-fastflix.sh

Amazing video processing software like handbrake but development is much faster.

## inst-laptop.sh

Enables brightness and battery blocks for laptops.

## inst-liquorix.sh

This is the install script from liquorix but modified to not install the kernel. It will result with the liquorix repository being added to /etc/apt/sources.list.d/.

## inst-lf.sh

Install the lf file manager from github repository.

## inst-makemkv.sh

A propietary movie decoder. You can install the beta license every thirty days or pay for it. Pay the guy if you got the money.

## inst-miscapps.sh

- gnome-mahjong

## inst-printer.sh

Installs cups and print drivers.

## inst-prodapps.sh

- atril: pdf viewer
- gnumeric: spreadsheet
- qutebrowser: vim inspired Internet browser

## inst-remmina.sh

Installs remmina remote desktop client and vnc and rdp plugins. 

## inst-sshd.sh

Installs OpenSSH server and updates firewall to permit connections. Accept the defaults or update /etc/ssh/sshd_conf.

## inst-touchpad-f9.sh

A terrible hack to toggle off touchpad on my Asus ROG G750. This is a work in progress.

## inst-virtmanager.sh

Installs virt-manager virtual machine manager using qemu and kvm. This is a work in progress.
