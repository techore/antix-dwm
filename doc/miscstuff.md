# Miscellaneous stuff

## git submodules

To add a submodule to a repository, cd to the location within the existing repository, them

```
git submodule add https://github.com/someone/repository
```

To clone a repository with submodules, execute

```
git clone --recuse-submodules https://gitlab.com/techore/antix-dwm
```

## test for arguments

Instead of testing for "", test for the number of arguments.

Example:

```
if test $# -eq 0 ; then
    assume a value and execute
    else
        use argument value and execute
fi
```

So if no argument is provide, execute an assumed/tested value, otherwise use argument.

## nvim

Sorting words on a line.

```
s/\v\s+/\r/g|'[,sort|,']j
```

:s/\s\+/\r/g    <--break the line into multiple ones
:'[,sort        <--sort them
:,']j           <--join them

## timezine

```
dpkg-reconfigure tzdata
```

## fish and sudo

/etc/fish/functions/sudo.fish

```
function sudo -d "sudo wrapper that handles aliases"
    if functions -q -- $argv[1]
        set -l new_args (string join ' ' -- (string escape -- $argv))
        set argv fish -c "$new_args"
    end

    command sudo $argv
end
```

## Create fish function

(need to experiment more with this. it does work but not always as expected)

Use `funced` to launch editor.

```
funced dmenu
```

Author the function, then funcsave to save.

```
funcsave dmenu
```

## clear sudo credentials

```
sudo -k
```

## Identifying packages 

In reusing antix-base solutions, it was necessary to identify what package provides a file.

For installed files:

```
dpkg-query -S $(which wpa_gui)
```

For files not installed:

```
sudo apt install apt-file
sudo apt-file update
sudo apt-file search [file]
```

## cifs shares

package: cifs-utils

credentials: /root/cifscred

```
username=bob
password=bobpasswd
```

fstab for read only:

```
//192.168.2.1/media /home/bob/media cifs ro,credentials=/root/.cifscred,uid=bob,gid=bob,iocharset=utf8,file_mode=0444,dirmode=0555 0 0
```

mkdir ~/media

## Screen resolution

This is incomplete for it doesn't take the current display device into consideration.

```
# Determine current resolution height, e.g. 1080, 1440, 2160, etc.
# $r is the resolution height | 1920x1080 results with 1080.
r=$(xrandr | grep current | awk '{print $10 }' | tr -d ",")

# Calculate the width of dialogue using w = $r * (300/1080).
# $r = 1080 results $w = 300, $r = 2160 results $w = 600.
w=$(printf "%.f" $(echo "$r * (300/1080)" | bc -l))
```

# Hard to find packages

poppler-utils --> pdftotext
