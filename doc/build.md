# Build

The purpose of this article is to document the antiX dwm build environment and process. The primary audience is me. 

Feedback in the way of suggestions or constructive criticism is appreciated. Please open an issue to initiate a discussion.

## High-level process

- I. Obtain materials
- II. Create install media
- III. Install build environment
- IV. Customize build environment
- V. Build antiX dwm
- VI. Create antiX dwm ISO
- VI. Upload ISO
- VII. Notify antiX community

## I. Obtain materials

I am using a laptop supporting both legacy BIOS (CSM) and UEFI. This is for convenience as well as to be able to test both BIOS and UEFI installs. In addition, I am using two USB flash drives. One is to install antiX Net on the laptop and the second is to write the antiX dwm spin for testing.

Current build materials are:

1. Asus X705UDR laptop
2. 2 x SanDisk Cruzer Glide 32GB USB 2 flash drives

Both materials exceed the minimum requirements needed to build antix-dwm respin. Any supported CPU can be used, it just will take longer to complete CPU intensive tasks. The USB can be as little as 1 GB, however, if customizing to support devices, I would recommend 4 GB or larger.

## II. Create install media

1. Existing antiX installation
2. Download antix-22-runit-net_x64-net.iso from https://antixlinux.com/download/
3. Insert usb flash drive
4. Execute `live-usb-maker`
5. Select "Make a full-featured live-usb" and complete USB creation

## III. Install build environment

1. Enable CMS (legacy) support in X705U BIOS
2. Boot computer and press F8 to select boot device
3. Select the antiX usb flash drive
4. Select the first boot entry, depress "e" and delete "spash=v" then ctrl+x 
5. Login as root using password root
6. Identify install target, `lsblk`, e.g. /dev/sda
7. Wipe larget disk, `wipefs -af /dev/sda`
8. Verify changes, `lsblk`
9. `reboot` to usb
10. Select the first boot entry, depress "e" and delete "spash=v" then ctrl+x. 
11. Login as root
12. Obtain IP address, `dhclient eth0`
13. Verify you have an IP address, `ip addr`
14. Set timezone, `dpkg-reconfigure tzdata`
15. Verify correct time, `date`, and if needed update, `date --set '2022-10-13 13:15:30'` then write to the hardware clock, `hwclock --systohc`
16. Begin antiX installation, `cli-installer`
17. Repartition disk: y
18. cfdisk will open for /dev/sda
19. Accept "gpt" for new partition table
20. Select "New" to create partition using all available space
21. Select "Write"
22. Type "yes" then **`enter`**
23. Review changes then "Quit"
24. root partion: sda1
25. Available file systesms are: ext4
26. Do you want to use a separate /home partition? n
27. Separate /home: n
28. Do you wish to perserve the data in the /home directory? n
29. Destroy data on sda1? y
30. antiX-net: y --> results with packages being downloaded, keep original config files if prompted
31. Do you want to install seatd? y
32. Do you want to install elogind and dbus-x11? n
33. cli-aptiX: n
34. Do you want to continue: y
35. Where should grub be installed? MBR 
36. Message: "Grub installed"
37. Computer name: axdwm
38. Set up system localization: n
39. Set up keyboard: n
40. Set up console layout: n
41. Set up system timezone: n
42. Is this a remastered/snapshot install: n
43. Set user account and password: delme
44. Set root account password
45. Set autologin for delme: n
46. "Installation of antiX finished!"
47. `poweroff` and remove USB drive

## IV. Customize build environment

1. Power on
2. Login as root
3. `apt update`
4. `apt install arch-install-scripts efibootmgr efivar git`
    - arch-install-scripts provides genfstab which is used for manual UEFI installs
    - efibootmgr is used to manage UEFI boot entries
    - utility used to manage UEFI variables
    ?- git to clone techore repos
5. `cd /usr/local/src/`
6. `git clone https://gitlab.com/techore/antix-dwm.git`
7. `poweroff`
8. Use clonezilla to backup clean install, e.g. 20230113-clean-[desc]
9. Done

On completion, poweroff, remove clonezilla usb, and power on.

1. Login as root
2. `apt upgrade && reboot`
3. Login as root
4. Use `cli-aptiX` to install newest 5.10 kernel
    - 5.10+ needed for 4.9 doesn't support statuscmd signaling correctly
5. `reboot`
6. Login and if no problems
7. `apt remove linux-image-4.9.0-326-antix.1-amd64-smp linux-headers-4.9.0-326-antix.1-amd64-smp`
8. `poweroff`
9. Use clonezilla to backup updated install, 20230114-update-[desc]
10. Done

On completion, poweroff, remove clonezilla usb, and power on.

## V. Build anitX dwm

1. Login as root
2. `cd /usr/local/src/antix-dwm`
3. ./build.sh
4. `reboot`
5. Login as root and if no problems
6. `userdel -r delme`
7. `adduser [user account]`
8. `poweroff`
9. Use clonezilla to backup antix-dwm release, 20230115-build-[desc]
10. Done

On completion, poweroff, remove clonezilla usb, and power on.

## VI. Create antiX dwm ISO

1. Login as techore
2. `cd /usr/local/src/antix-dwm/build/`
3. `sudo ./create-iso.sh`
4. create-iso.sh will complete and save the ISO in /home/snapshot/
5. Done

Next, use live usb maker to create install usb to test.

1. Insert second USB flash drive
2. `sudo live-usb-maker`
    - Make a full-featured live-usb
    - Copy from an ISO file
    - Please enter filename: /home/snapshot/[ISO]
    - Is this correct: yes
    - Check md5 of the file? yes
    - Shall we begin? yes
3. Done

## VII. Upload ISO

1. Login to https://archive.org
2. Select "Upload" in the upper-right corner
3. Select "Upload Files" button
4. Drag and drop files or select "Choose file to upload" button
5. Add metatags
6. Add description
7. Submit

## VIII. Notify antiX community

1. Login to https://antixforum.com
2. Create or update post under antiX-development --> antiX Respins

- (antix-22-runit-dwm_x64)[https://www.antixforum.com/forums/topic/antix-22-dwm-unofficial-spin/#post-97294]

## **Notes**

1. Deleting "splash=v" is to prevent a stall at "Waiting for /dev to be fully populated..." which happens on 25% or more of my computers.
2. Extraneous UEFI boot entries can be removed at or after installation using efibootmgr.
   - List boot entries, `efibootmgr`
   - Identify unwanted boot entries, e.g. 0000 Windows 10
   - Delete a boot entry, `efibootmgr -B -b 0000`
3. The laptop has a second storage device. It is formated ext4 and is used to store clonezilla images of the project.
