#!/bin/env bash
# Project: antix-dwm
# Location: antix-dwm/build.sh
# Dependency: clone of antix-dwm repository
# Description: Primary build script to prepare antix-dwm ISO.
# BRE escape characters: $.*[\^
# Usage: sudo ./build.sh - ensure you are in the antix-dwm directory before executing.

cwd=$PWD	  # Current Working Directory
sn=0		  # Step Number

shopt -s dotglob  # Set to copy dot files

# Verify execution as root user.
if test $EUID -ne 0; then
    printf  "\n    Must be root!\n\n"
    exit 1
fi

# Install build.sh package dependencies
printf "\n=========>  Install build.sh package dependencies\n\n"
apt update
apt -y install wget ntpdate

# Verify network connectivity and name resolution.
wget -q --tries=5 --timeout=10 --spider http://www.ntp.org
if test $? -ne 0; then
    printf "\n=========>  Fix your network connectivity then execute build.sh!\n\n"
    exit 1
fi

# Verify system time is plus or minus five minutes.
max_offset=300
ntp_offset=$(ntpdate -q 0.pool.ntp.org | head -1 | cut -d " " -f 6 | sed 's/-//' | sed 's/\.[0-9]*,//')
if test $ntp_offset -gt $max_offset; then
    printf "\n=========>  Fix your computer time then execute build.sh!\n\n"
    printf '\n=========>  sudo date --set "2022-10-29 22:29:30"\n'
    printf "=========>  sudo hwclock --systohc\n\n"
    exit 1
fi

# Firewall
printf  "\n\n=========> Build step $((sn+=1)):"
printf  "  Enabling firewall\n\n"
ufw enable
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# prebuild-mpr repository
#printf "\n=========> Build step $((sn+=1)):"
#source "$cwd/build/inst-prebuilt-mpr.sh"
#printf  "\n=========> Step $sn complete.\n"
#read -p "Enter to continue or ctrl+c to exit: "

# Update apt cache
printf "\n=========> Build step $((sn+=1)):"
printf "  Updating apt cache\n\n"
apt update
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Upgrade packages
printf "\n=========> Build step $((sn+=1)):"
printf "  Upgrading packages\n\n"
apt --yes upgrade
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# System tools packages
printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/pkgs-systools.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Xorg packages
printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/pkgs-xorg.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Xorg configuration
printf "\n=========> Build step $((sn+=1)):"
printf "  Enabling Xorg DontZap\n\n"
cp "$cwd/file/xorg.conf.d/"* /etc/X11/xorg.conf.d/
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Xwrapper.config
printf "\n=========> Build step $((sn+=1)):"
printf "  Updating Xwrapper.config\n\n"
if ! grep -q "needs_root_rights=yes" /etc/X11/Xwrapper.config; then
  printf  "needs_root_rights=yes\n" >> /etc/X11/Xwrapper.config
fi
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Suckless build dependencies
printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/pkgs-suckless.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Xorg system tools packages
printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/pkgs-xsystools.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# UI related packages
printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/pkgs-ui.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# ALSA tools packages
printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/pkgs-audio.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Remove obsolete packages
printf "\n=========> Build step $((sn+=1)):"
printf "  Removing obsolete packages\n\n"
apt --yes autoremove
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Download dwm et al sources
printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/git-dwm.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/git-dwmblocks.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/git-slock.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/git-st.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Download and install themes
printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/git-themes.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Download and install dircolors
printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/git-dircolors.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# QT_QPA_PLATFOMRTHEME 
printf "\n=========> Build step $((sn+=1)):"
printf "  Setting QT_QPA_PLATFOMRTHEME /etc/environment\n\n"
if ! grep "QT_QPA_PLATFORMTHEME" /etc/environment; then
    printf 'QT_QPA_PLATFORMTHEME="qt5ct"\n' >> /etc/environment
fi
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# sudoers
printf "\n=========> Build step $((sn+=1)):"
printf "  Copying axdwmers to /etc/sudoers.d./\n\n"
cp -f "$cwd/file/sudoers.d/axdwmers" /etc/sudoers.d/
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Fonts
printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/font-material_icons.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/font-nerd_fonts.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/font-source_code.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/font-source_sans.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/font-source_serif.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

printf "\n=========> Build step $((sn+=1)):"
printf "  Updating font cache\n\n"
fc-cache -fv
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Set terminal
printf "\n=========> Build step $((sn+=1)):"
printf "  Setting kitty as default terminal\n\n"
update-alternatives --set x-terminal-emulator $(which kitty)
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Copy kitty files
printf "\n=========> Build step $((sn+=1)):"
printf "  Copying kitty files\n\n"
cp -fr "$cwd/file/kitty/"* /etc/xdg/kitty/
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Set default shell 
printf "\n=========> Build step $((sn+=1)):"
printf "  Setting shell for user accounts\n\n"
sed -i 's/#DSHELL=\/bin\/bash/DSHELL=\/usr\/bin\/fish/' /etc/adduser.conf
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Copy fish files
printf "\n=========> Build step $((sn+=1)):"
printf "  Copying fish files\n\n"
cp -r "$cwd/file/fish/"* /etc/fish/
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Install oh-my-fish fenv files
printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/git-fenv.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# 70-persistent-net.rules
printf "\n=========> Build step $((sn+=1)):"
printf "  Reseting persistent network rules\n\n"
rm -f /etc/udev/rules.d/70-persistent-net.rules
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# lf
#printf "\n=========> Build step $((sn+=1)):"
#source "$cwd/build/inst-lf.sh"
#printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# anacron 
printf "\n=========> Build step $((sn+=1)):"
printf "  Copying anacron apt job\n\n"
cp "$cwd/file/cron.daily/"* /etc/cron.daily/
#cp "$cwd/file/cron.weekly/"* /etc/cron.weekly/
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# rofi themes 
printf "\n=========> Build step $((sn+=1)):"
printf "  Copying rofi themes\n\n"
cp "$cwd/file/themes/"* /usr/share/rofi/themes/
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Neovim 8.0
printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/inst-neovim.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Remove packages 
printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/rm-packages.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Delete live-files 
printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/rm-livefiles.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

printf "\n=========> Build step $((sn+=1)):"
printf "  Copying neovim wrappers to libexec\n\n"
if test ! -d /usr/libexec/neovim; then
    mkdir /usr/libexec/neovim
fi
cp "$cwd/file/neovim/"* /usr/libexec/neovim/
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Binaries
# 2023Feb28: on next antix-goodies package update, verify
# titles are set in yad-calendar and yad-volume. If true,
# delete yad-calendar and yad-volume from files/bin/.
# merge: cfca03451b39b879d19db271656b370805c3cf70
# merge: 783976fc21214e9d42611b9d3241d1605285d37c
printf "\n=========> Build step $((sn+=1)):"
printf "  Copying bin files to /usr/local/bin/\n\n"
cp "$cwd/file/bin/"* /usr/local/bin/
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# profile.d
printf "\n=========> Build step $((sn+=1)):"
printf "  Copying profile files to /etc/profile.d/\n\n"
cp "$cwd/file/profile.d/"* /etc/profile.d/
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# bash.bashrc
printf "\n=========> Build step $((sn+=1)):"
printf "  Adding aliases to /etc/bash.bashrc\n\n"
if ! grep "# axdwm-aliases" /etc/bash.bashrc ; then
    cat "$cwd/build/bash_aliases" >> /etc/bash.bashrc
fi
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# etc
printf "\n=========> Build step $((sn+=1)):"
printf "  Copying etc files to /usr/local/etc/\n\n"
cp -r "$cwd/file/etc/"* /etc/
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# local/etc
printf "\n=========> Build step $((sn+=1)):"
printf "  Copying local-etc files to /usr/local/etc/\n\n"
cp -r "$cwd/file/local-etc/"* /usr/local/etc/
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# conky 
printf "\n=========> Build step $((sn+=1)):"
printf "  Copying conky files to /etc/conky/\n\n"
if test ! -d /etc/conky; then
    mkdir /etc/conky
fi
cp "$cwd/file/conky/"* /etc/conky/
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# skel
printf "\n=========> Build step $((sn+=1)):"
printf "  Copying skel files to /etc/skel/\n\n"
rm -fr /etc/skel/*
cp -r "$cwd/file/skel/"* /etc/skel/
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# NvChad
printf "\n=========> Build step $((sn+=1)):"
source "$cwd/build/inst-nvchad.sh"
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# skel --> /root/
printf "\n=========> Build step $((sn+=1)):"
printf "  Copying skel files to /root/\n\n"
rm -fr /root/*
cp -r "/etc/skel/"* /root/
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Copying vifm files
printf "\n=========> Build step $((sn+=1)):"
printf "  Copying vifm files to //\n\n"
cp -fr "$cwd/file/vifm/"* /usr/share/vifm/
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# wallpapers
printf "\n=========> Build step $((sn+=1)):"
printf "  Copying antix-dwm wallpapers to /usr/share/wallpaper/axdwm/\n\n"
rm -fr /usr/share/wallpaper/axdwm/
cp -r "$cwd/file/wallpaper" /usr/share/wallpaper/axdwm
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# aptupg.out
printf "\n=========> Build step $((sn+=1)):"
printf "  Creating /var/tmp/aptupg.out\n\n"
printf "0\n" > /var/tmp/aptupg.out
chmod 666 /var/tmp/aptupg.out
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Configuring, compiling, and copying dwm, dwmblocks, and slock binaries
printf "\n=========> Build step $((sn+=1)):"
printf "  Installing dwm\n\n"
/usr/local/bin/conf-dwm.sh /usr/local/src/dwm-flexipatch
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

printf "\n=========> Build step $((sn+=1)):"
printf "  Installing dwmblocks\n\n"
/usr/local/bin/conf-dwmblocks.sh /usr/local/src/dwmblocks
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

printf "\n=========> Build step $((sn+=1)):"
printf "  Installing slock\n\n"
/usr/local/bin/conf-slock.sh /usr/local/src/slock-flexipatch
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

printf "\n=========> Build step $((sn+=1)):"
printf "  Installing st\n\n"
/usr/local/bin/conf-st.sh /usr/local/src/st-flexipatch
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Services
printf "\n=========> Build step $((sn+=1)):"
printf "  Enabling services\n\n"
ln -s /etc/sv/cron /etc/runit/runsvdir/default/cron
ln -s /etc/sv/anacron /etc/runit/runsvdir/default/anacron
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# root chsh
printf "\n=========> Build step $((sn+=1)):"
printf "  Changing root's shell to fish\n\n"
chsh -s $(which fish)
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Fix ping for users 
printf "\n=========> Build step $((sn+=1)):"
printf "  Fix ping for users\n\n"
setcap cap_net_raw+ep /usr/bin/ping
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Disable connman url checks
printf "\n=========> Build step $((sn+=1)):"
printf "  Disable connman url checks\n\n"
sed -i 's/# EnableOnlineCheck = false/EnableOnlineCheck = false/' /etc/connman/main.conf
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Configure runit logs
printf "\n=========> Build step $((sn+=1)):"
printf "  Configure runit logs\n\n"
# anacron
mkdir /var/log/runit
mkdir /etc/sv/anacron/log
cp -f "$cwd/file/runit/anacron/run" /etc/sv/anacron/log/
mkdir /var/log/runit/anacron
# connman
mkdir /etc/sv/connman/log
cp -f "$cwd/file/runit/connman/run" /etc/sv/connman/log/
mkdir /var/log/runit/connman
# cron
mkdir /etc/sv/cron/log
cp -f "$cwd/file/runit/cron/run" /etc/sv/cron/log/
mkdir /var/log/runit/cron
# dbus
mkdir /etc/sv/dbus/log
cp -f "$cwd/file/runit/dbus/run" /etc/sv/dbus/log/
mkdir /var/log/runit/dbus
# dhclient
mkdir /etc/sv/dhclient/log
cp -f "$cwd/file/runit/dhclient/run" /etc/sv/dhclient/log/
mkdir /var/log/runit/dhclient
# haveged
mkdir /etc/sv/haveged/log
cp -f "$cwd/file/runit/haveged/run" /etc/sv/haveged/log/
mkdir /var/log/runit/haveged
# rsync
mkdir /etc/sv/rsync/log
cp -f "$cwd/file/runit/rsync/run" /etc/sv/rsync/log/
mkdir /var/log/runit/rsync
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

# Recent change breaks customization of default shell for
# initial user when using antiX install.
# Change: https://gitlab.com/antiX-Linux/remaster/-/commit/3c0f5dfd2ce96915cf626a79328012870cf36edb
printf "\n=========> Build step $((sn+=1)):"
printf "  Changing fix /usr/sbin/installed-to-live\n\n"
sed -i 's/--shell "\${DEF_SHELL:-\/usr\/bin\/bash}" //' /usr/sbin/installed-to-live
printf  "\n=========> Step $sn complete.\n"
read -p "Enter to continue or ctrl+c to exit: "

## Done!
printf "\n=========> build.sh is complete!  \n\n"

read -s -n 1 -p "Enter 1 to reboot, 2 to poweroff, or 3 to exit" askrpe
printf "\n"

case "$askrpe" in
    1) reboot ;;
    2) poweroff ;;
    *) exit 0 ;;
esac
