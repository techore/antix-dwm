#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/rofi-favmenu.sh
# Dependencies: rofi and defined programs.
# Description: prompt user to select defined programs
# Usage:
#     Called using dwm keybinding mod+p.
#     Edit for your preferred programs. rofi theme programs.rasi theme may need to be udpated, e.g. lines. 

chosen=$(printf "makemkv\nfastflix\ntmdb\ntvdb\nmarktext" | rofi -dmenu -i -theme-str '@import "favorites.rasi"')

case "$chosen" in
    "makemkv") makemkv ;;
    "fastflix") FastFlix ;;
    "tmdb") xdg-open "https://www.themoviedb.org/" ;;
    "tvdb") xdg-open "https://thetvdb.com/search" ;;
    "marktext") marktext ;;
    *) exit ;;
esac
