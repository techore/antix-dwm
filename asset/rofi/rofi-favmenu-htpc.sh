#!/usr/bin/env bash
# Project: antix-dwm
# Location: /usr/local/bin/rofi-favmenu.sh
# Dependencies: rofi and defined programs.
# Description: prompt user to select defined programs
# Usage:
#     Called using dwm keybinding mod+p.
#     Edit for your preferred programs. rofi theme programs.rasi theme may need to be udpated, e.g. lines. 

chosen=$(printf "Kodi\nNetflix\nAmazon Prime\nDisney+\nYoutube FZF" | rofi -dmenu -i -theme-str '@import "favorites.rasi"')

case "$chosen" in
    "Kodi") /usr/bin/kodi -fs ;;
    "Netflix") xdg-open "https://www.netflix.com" ;;
    "Amazon Prime") xdg-open "https:/www.amazon.com/Amazon-Video/b?node=2858778011" ;;
    "Disney+") xdg-open "https://www.disneyplus.com/home" ;;
    "Youtube FZF") kitty --title youtube-fzf ytfzf -t -T kitty ;;
    *) exit ;;
esac
